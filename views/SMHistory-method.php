<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User-Dashboard</title>

    <!-- Favicons -->
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">

    <!-- Bootstrap core CSS-->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../dist/lib/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="../dist/lib/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../dist/lib/bootstrap/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../dist/lib/css/ratingForm.css">

</head>

<body id="page-top">

<!-- Requiring navBar-->
<?php require_once 'userNavBar.php'; ?>

<div id="wrapper">

    <!-- Sidebar -->

    <!-- Requiring side Bar panel-->
    <?php require_once 'userSideBar.php'; ?>

    <div id="content-wrapper">

        <div class="container-fluid" style="margin-top: 2.9rem">

            <div class="card mb-3" style="background-color: lightskyblue">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Reservation History
                </div>
            </div>

            <!-- Page Content -->
            <hr>
            <h4 class="text-secondary"></h4>
            <hr>
            <?php require_once 'SMHistory.php'; ?>


        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->

        <?php
        require_once 'footer.php';
        ?>
    </div>
    <!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>-->
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="logout">Logout</a>
            </div>
        </div>
    </div>
</div>
<!-- modal for rating-->
<div class="modal fade" id="ratingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <hr>
            <div class="modal-body text-center">
                <div class="stars">
                    <form action="">
                        <input class="star star-5" id="star-5" type="radio" name="star"/>
                        <label class="star star-5" for="star-5"></label>
                        <input class="star star-4" id="star-4" type="radio" name="star"/>
                        <label class="star star-4" for="star-4"></label>
                        <input class="star star-3" id="star-3" type="radio" name="star"/>
                        <label class="star star-3" for="star-3"></label>
                        <input class="star star-2" id="star-2" type="radio" name="star"/>
                        <label class="star star-2" for="star-2"></label>
                        <input class="star star-1" id="star-1" type="radio" name="star"/>
                        <label class="star star-1" for="star-1"></label>

                    </form>

                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Your Comment.</label>
                    <textarea class="form-control" id="commentArea" rows="3"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="submit-rating" type="button" class="btn btn-success">Submit</button>
            </div>
        </div>
    </div>
</div>

<script>
    let userName = "<?php echo $id ?>";
</script>
<!-- Bootstrap core JavaScript-->
<script src="../dist/lib/jquery/jquery.min.js"></script>
<script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"
        integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i"
        crossorigin="anonymous"></script>
<script src="../dist/controller/configuration.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!--<script src="../dist/controller/ratingForm.js"></script>-->

<script src="../dist/controller/employeeHistoryController.js"></script>



</body>

</html>
