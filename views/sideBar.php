


<ul class="sidebar navbar-nav fixed-nav" style="margin-top: 3.5rem;">
    <li class="nav-item active">
        <a id="adminSidebarDashboard" class="nav-link">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
<!--    <li class="nav-item dropdown">-->
<!--        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown"-->
<!--           aria-haspopup="true" aria-expanded="false">-->
<!--            <i class="fas fa-fw fa-folder"></i>-->
<!--            <span>Pages</span>-->
<!--        </a>-->
<!--        <div class="dropdown-menu" aria-labelledby="pagesDropdown">-->
<!--            <h6 class="dropdown-header">Login Screens:</h6>-->
<!--            <a class="dropdown-item" href="login.php">Login</a>-->
<!--            <a class="dropdown-item" href="register.php">Register</a>-->
<!--            <a class="dropdown-item" href="forgot-password.php">Forgot Password</a>-->
<!--            <div class="dropdown-divider"></div>-->
<!--            <h6 class="dropdown-header">Other Pages:</h6>-->
            <!--                <a class="dropdown-item" href="dist/404.html">404 Page</a>-->
<!--            <a class="dropdown-item" href="blank.php">Blank Page</a>-->
<!--        </div>-->
<!--    </li>-->
    <li class="nav-item">
        <a id="sideBarManageUser" class="nav-link">
            <i class="fas fa-user"></i>
            <span>Manage Employee</span></a>
    </li>

     <li class="nav-item">
            <a id="sideBarManageRevenue" class="nav-link">
                <i class="fas fa-user"></i>
                <span>Add Revenue</span></a>
        </li>

    <li class="nav-item">
        <a id="sidebarCompartment" class="nav-link">
            <i class="fas fa-boxes"></i>
            <span>Compartment <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Request</label></span></a>
    </li>
    <li class="nav-item">
        <a id="sidebarTrainRequest" class="nav-link">
            <i class="fas fa-train"></i>
            <span>Train Request</span></a>
    </li>

    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
           aria-expanded="true" style="cursor: pointer">
            <i class="fas fa-chart-pie"></i>
            <span>Reservation Reports</span>
        </a>
        <div class="dropdown-menu bg-dark dropReport" aria-labelledby="pagesDropdown">

            <a id="sidebarSeatReserve" class="dropdown-item text-white">
                <i class="fas fa-chair float-right"></i>
                <span>Seats</span></a>

            <a id="sidebarTrainReser" class="dropdown-item text-white">
                <i class="fas fa-subway float-right"></i>
                <span>Trains</span></a>

            <a id="sidebarCompartmentRes" class="dropdown-item text-white">
                <i class="fas fa-archway float-right"></i>
                <span>Compartments</span></a>

        </div>
    </li>

    <li class="nav-item">
        <a id="sidebarLogout" class="nav-link">
            <i class="fas fa-sign-out-alt"></i>
            <span>Log Out</span>
        </a>
    </li>
</ul>