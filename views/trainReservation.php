<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta data and browser settings -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Page title -->
    <title>User - Dashboard</title>

    <!-- Link to the favicon -->
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">

    <!-- Bootstrap CSS library -->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Fontawesome for the icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="../dist/lib/css/core.min.css">
    <link href="../dist/lib/css/style.css" rel="stylesheet">
    <link href="../dist/lib/bootstrap/css/sb-admin.min.css" rel="stylesheet">
    <link href="../dist/lib/css/seatBookStyle.css" rel="stylesheet">
</head>

<body id="page-top">

<!-- Including navigation bar -->
<?php require_once 'userNavBar.php'; ?>

<div id="wrapper">

    <!-- Sidebar -->

    <!-- Requiring side Bar panel-->
    <?php require_once 'userSideBar.php' ?>

    <div id="content-wrapper">
        <!-- Main content starts here -->
        <div class="container" style="margin-top: 2.9rem">

            <!-- Card to contain form for filter train reservation -->
            <div class="card mb-3" style="background-color: lightskyblue">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Train Reservation
                </div>
            </div>

            <section id="about">

                <div class="card">
                    <div class="card-body">
                        <!-- Reservation filtration form -->
                        <div class="row">
                            <h3>Reserve a Train in long distance train across Sri Lanka</h3>
                        </div>

                        <div class="row">

                            <div class="col-md-4 mb-3">
                                <label for="resturantName">Starting From</label>

                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-search-location"
                                                                      data-toggle="tooltip" data-placement="top"
                                                                      title="Press enter after enter the city..!"></i></span>
                                    <input id="startCity" type="text" class="form-control "
                                           name="resturantName"
                                           placeholder="Type city" data-toggle="tooltip" data-placement="top"
                                           title="Press enter after enter the city..!" required>

                                </div>
                                <div class="input-group-append mt-1">
                                    <label class="text-danger ">**Main Station : </label>
                                    <label class="ml-2" id="mainStation1"></label>
                                </div>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="resturantName">Going To</label>

                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-search-location"
                                                                      data-toggle="tooltip" data-placement="top"
                                                                      title="Press enter after enter the city..!"></i></span>
                                    <input type="text" class="form-control " id="destination"
                                           name="resturantName"
                                           placeholder="Type your destination" data-toggle="tooltip"
                                           data-placement="top" title="Press enter after enter the city..!" required>

                                </div>
                                <div class="input-group-append mt-1">
                                    <label class="text-danger ">**Main Station : </label>
                                    <label class="ml-2" id="mainStation2"></label>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="resturantName">Journey Date</label>

                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    <input type="date" class="form-control " id="reservationDate"
                                           name="resturantName"
                                           placeholder="Type your destination" required>

                                </div>
                            </div>

                            <div class="row align-content-center">
                                <div class="container">
                                    <button class="btn btn-primary m-2" id="userSearchTrain" type="button">Search Train
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </section>

            <!-- Table to display the search result of trains -->
            <div class="container table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-center bg-secondary text-white">Train Name</th>
                        <th class="text-center bg-secondary text-white">From</th>
                        <th class="text-center bg-secondary text-white">TO</th>
                        <th class="text-center bg-secondary text-white">Arrival Time</th>
                        <th class="text-center bg-secondary text-white">Departure Time</th>
                        <th class="text-center bg-secondary text-white">Action</th>

                    </tr>
                    </thead>
                    <tbody id="userShedule">

                    </tbody>

                </table>
            </div>

        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
    </div>
    <?php
    require_once 'footer.php';
    ?>
    <!-- /.content-wrapper -->


    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Train Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row text-center">
                        <label class="form-control col-4 text-center">Number Of Seats</label>
                        <input type="text" class="form-control col-2 text-center" id="numberOfSeat"
                               name="resturantName">
                        <label class="form-control col-3 text-center">Amount</label>
                        <input type="text" class="form-control col-3 text-center" id="amount" name="amount">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="user-send-request" type="button" class="btn btn-primary">Send Request</button>
                </div>
            </div>
        </div>
    </div>

    <!-- /#wrapper -->

    <!--Payment Modal-->
    <div class="modal fade" id="requestModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <!--                    Request modal-->
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Accepting Request..!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">User Name :</label>
                                <input type="text" class="form-control" id="recipient-name" placeholder="User Name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Message :</label>
                                <textarea class="form-control" id="message-text" placeholder="comment"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Send message</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <!--            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>-->
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" id="logout">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <script>
        let userName = "<?php echo $id ?>";
    </script>
    <!-- Bootstrap core JavaScript-->
    <script src="../dist/lib/jquery/jquery.min.js"></script>
    <script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"
            integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i"
            crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="../dist/controller/configuration.js"></script>
    <script src="../dist/controller/seatBookController.js"></script>
    <script src="../dist/controller/userReservations.js"></script>
    <script src="../dist/controller/passengerController.js"></script>
    <script src="../dist/controller/userProfileController.js"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>


    <script>
    let userName = "<?php echo $id ?>";
</script>
<!-- JavaScript files and scripts -->
<script src="../dist/lib/jquery/jquery.min.js"></script>
<script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"
        integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../dist/controller/configuration.js"></script>
<script src="../dist/controller/seatBookController.js"></script>
<script src="../dist/controller/userReservations.js"></script>
<script src="../dist/controller/passengerController.js"></script>
<script src="../dist/controller/userProfileController.js"></script>
<script>
    // Tooltips initialization
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>
</body>
</html>