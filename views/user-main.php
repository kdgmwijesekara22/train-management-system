<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Railway System</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">
    <!--    <link href="../dist/img/apple-touch-icon.png" rel="apple-touch-icon">-->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700"
          rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="../dist/lib/animate/animate.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="../dist/lib/css/style.css" rel="stylesheet">
    <link href="../dist/lib/css/core.min.css" media="screen" rel="stylesheet" type="text/css">

    <link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet"
          type="text/css"/>


</head>

<body>

<header id="header">
    <div class="container">
        <div id="logo" class="pull-left">
            <a href="#hero"><img src="" alt="" title=""/></img></a>

        </div>

        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="#hero">Home</a></li>
                <li><a href="#about">Search Trains</a></li>
<!--                <li><a href="#team">Hotels</a></li>-->
<!--                <li><a href="#services">Stations</a></li>-->
                <li><a id="trainResLogin">Log in</a></li>

            </ul>
        </nav><!-- #nav-menu-container -->
    </div>
</header><!-- #header -->

<!--==========================
  Hero Section
============================-->
<section id="hero">
    <div class="hero-container">
        <h1 >Welcome to Railway System</h1>
        <h2 style="font-family: 'Brush Script MT'; font-size: 55px">We help to make your journey Easy... </h2>
        <a href="#about" class="btn-get-started">Book Your Journey</a>
    </div>
</section><!-- #hero -->

<main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
        <div class="container">
            <h1 style="font-family: 'Lucida Handwriting'">Trains</h1>
            <Pre>
            </Pre>
            <div class="row">
                <div class="card-deck">
                    <div class="card shadow-lg">
                        <img src=../dist/img/ruhunuKumari.jpg style="height: 200px" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Ruhunu Kumari</h5>
                            No. 8058 & 8059<br>
                            Maradana to Matara (Runs via Colombo-fort)
                        </div>
                        <div class="card-footer text-center">
                            <a id="userMainReserveNow">
                                <button class="btn btn-success m-2" type="button">Reserve Now</button>
                            </a>

                        </div>
                    </div>
                    <div class="card shadow-lg">
                        <img src="../dist/img/galuKumari.jpg" style="height: 200px" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title " style="font-family: 'Comic Sans MS'">Galu Kumari</h5>
                            No. 8056 & 8057<br>
                            Maradana to Mathara (Runs via Colombo-fort)
                        </div>
                        <div class="card-footer text-center">
                            <a id="userMainReserveNowCard">
                                <button class="btn btn-success m-2" type="button">Reserve Now</button>
                            </a>

                        </div>
                    </div>
                    <div class="card shadow-lg">
                        <img src="../dist/img/ruhunuKumari.jpg" style="height: 200px" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Sagarika</h5>
                            No. 8096 & 8097<br>
                            Colombo (Maradana) to Mathara
                        </div>
                        <div class="card-footer text-center">
                            <a id="userMainReserveNowCardMaradana">
                                <button class="btn btn-success m-2" type="button">Reserve Now</button>
                            </a>

                        </div>
                    </div>
                </div>
            </div>

            <br>

            <div class="row">
                <div class="card-deck">
                    <div class="card shadow-lg">
                        <img src="../dist/img/galuKumari.jpg" style="height: 200px" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">KKS Intersity</h5>
                            No. 4017 & 4018.<br>
                            Operates between Colombo and Kankesanturai
                        </div>
                        <div class="card-footer text-center">
                            <a id="userMainReserveNowCol-Kankas">
                                <button class="btn btn-success m-2" type="button">Reserve Now</button>
                            </a>

                        </div>
                    </div>
                    <div class="card shadow-lg">
                        <img src="../dist/img/Class-M10.jpg" style="height: 200px" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title " style="font-family: 'Comic Sans MS'">Yal-Devi</h5>
                            No. 4001 & 4002<br>
                            Mount-Lavinia to Kankasanthurai (Runs via Jaffna)
                        </div>
                        <div class="card-footer text-center">
                            <a id="userMain-ReserveNow-MountLav">
                                <button class="btn btn-success m-2" type="button">Reserve Now</button>
                            </a>

                        </div>
                    </div>
                    <div class="card shadow-lg">
                        <img src="../dist/img/Matara-Railway-Station.jpg" style="height: 200px" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Night-Mail</h5>
                            No. 1005 & 1006<br>
                            Colombo to Badulla
                        </div>
                        <div class="card-footer text-center">
                            <a id="userMain_reserveNow_Badulla">
                                <button class="btn btn-success m-2" type="button">Reserve Now</button>
                            </a>

                        </div>
                    </div>
                </div>
            </div>

    </section>
    <!-- #about -->


    <!--==========================
      Team Section
    ============================-->
    <!--    <section id="team">-->
    <!--        <div class="container">-->
    <!--            <h1 style="font-family: 'Lucida Handwriting'">Hotels</h1>-->
    <!--            <Pre>-->
    <!--            </Pre>-->
    <!--            <div class="row">-->
    <!--                <div class="card-deck">-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/hotel/cape.jpg " style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'"><B>Cape Resort</B> </h5>-->
    <!--                            <p>Located on Beach Avenue, the newly-renovated Beach Shack has the feel of a fresh island retreat. Now open through December</p>-->
    <!---->
    <!--                        </div>-->
    <!--                        <div class="card-footer text-center">-->
    <!--                            <a id="userMainReserveNow_Loacted">-->
    <!--                                <button class="btn btn-success m-2" type="button">Reserve Now</button>-->
    <!--                            </a>-->
    <!---->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/hotel/Marriott3.jpg" style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title " style="font-family: 'Comic Sans MS'">Marriot</h5>-->
    <!--                            <p>The awe-inspiring beauty of Sri Lanka is on full display at Weligama Bay Marriott Resort & Spa. Located on Weligama Bay, moments from Mirissa Beach</p>-->
    <!--                        </div>-->
    <!--                        <div class="card-footer text-center">-->
    <!--                            <a id="userMain_ReserveNow_Marroit">-->
    <!--                                <button class="btn btn-success m-2" type="button">Reserve Now</button>-->
    <!--                            </a>-->
    <!---->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/hotel/fishermans-bay.jpg" style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Fisherman Bay Resort</h5>-->
    <!--                            <p>At Bay Beach Hotel, you will find a garden, barbeque facilities and a cosy outdoor terrace. Guests can also utilize the meeting facilities and luggage storage space. </p>-->
    <!---->
    <!--                        </div>-->
    <!--                        <div class="card-footer text-center">-->
    <!--                            <a id="userMain_ReserveNow_Fiserman">-->
    <!--                                <button class="btn btn-success m-2" type="button">Reserve Now</button>-->
    <!--                            </a>-->
    <!---->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!---->
    <!--            <br>-->
    <!---->
    <!--            <div class="row">-->
    <!--                <div class="card-deck">-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/hotel/88902727.jpg" style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Palm Resort</h5>-->
    <!--                            <p>Located in Weligama, just a short stroll from the nearest beach, Bay Beach Hotel features an in-house restaurant with a variety of international cuisines .</p>-->
    <!--                        </div>-->
    <!--                        <div class="card-footer text-center">-->
    <!--                            <a id="userMain_reserveNow_PalmResort">-->
    <!---->
    <!--                                <button class="btn btn-success m-2" type="button">Reserve Now</button>-->
    <!--                            </a>-->
    <!---->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/hotel/facade-from-beach.jpg" style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title " style="font-family: 'Comic Sans MS'">Amari</h5>-->
    <!--                          <p>Located in the historic fort city of Galle on the southern coast of Sri Lanka, Amari Galle is a luxury resort featuring rooms and  an outdoor pool with a pool bar and fitness centre.-->
    <!--                        </div>-->
    <!--                        <div class="card-footer text-center">-->
    <!--                            <a id="userMain_reserveNow_amari">-->
    <!--                                <button class="btn btn-success m-2" type="button">Reserve Now</button>-->
    <!--                            </a>-->
    <!---->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/hotel/resorts.jpg" style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Rest Resort</h5>-->
    <!--                            <p><p>Located in Weligama, just a short stroll from the nearest beach, Bay Beach Hotel features an in-house restaurant with a variety of international cuisines. </p></p>-->
    <!--                        </div>-->
    <!--                        <div class="card-footer text-center">-->
    <!--                            <a id="userMain_reserveNow_restResort">-->
    <!---->
    <!--                                <button class="btn btn-success m-2" type="button">Reserve Now</button>-->
    <!--                            </a>-->
    <!---->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!---->
    <!---->
    <!--    </section>-->


    <!--==========================
     Services Section
   ============================-->
    <!--    <section id="services">-->
    <!--        <div class="container">-->
    <!--            <h1 style="font-family: 'Lucida Handwriting'">Station</h1>-->
    <!--            <Pre>-->
    <!--            </Pre>-->
    <!--            <div class="row mt-2">-->
    <!--                <div class="card-deck">-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/station/anuradhapura.jpg " style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Anuradhapura </h5>-->
    <!--                            <p>It is the capital city of North Central Province, Sri Lanka and the capital of Anuradhapura District.</p>-->
    <!--                        </div>-->
    <!---->
    <!--                    </div>-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/station/bandarawela-railway-station.jpg" style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Bandarawela</h5>-->
    <!--                            <p>The construction of the railway line between Haputale and Bandarawela commenced in 1887 but it wasn't until 1893 that work on the line was completed.</p>-->
    <!--                        </div>-->
    <!---->
    <!--                    </div>-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/station/Badulla%20railway%20station%7Bterminus)%20(11).JPG" style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Badulla</h5>-->
    <!--                            <p>The construction of the line from Nanu Oya to Badulla was completed in 1924, with the passenger traffic first commencing on 5 February 1924.</p>-->
    <!--                        </div>-->
    <!---->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!---->
    <!--            <div class="row mt-2">-->
    <!--                <div class="card-deck">-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/station/colombo.jpg " style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Colombo</h5>-->
    <!--                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>-->
    <!---->
    <!--                        </div>-->
    <!---->
    <!--                    </div>-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/station/galle.jpg" style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title"style="font-family: 'Comic Sans MS'">Galle</h5>-->
    <!--                            <p class="card-text">Galle Station is located in the centre of Galle. It is just northwest of the Galle International Cricket Ground. Galle's City Hall is next to the station.</p>-->
    <!--                        </div>-->
    <!---->
    <!--                    </div>-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/station/hikkaduwa.jpg" style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Hikkaduwa</h5>-->
    <!--                            <p class="card-text">Hikkaduwa is a small town on the south coast of Sri Lanka located in the Southern Province, about 17 km (11 mi) north-west of Galle and 98 km (61 mi) south of Colombo.</p>-->
    <!--                        </div>-->
    <!---->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="row mt-2">-->
    <!--                <div class="card-deck">-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/station/ellla.jpg " style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Demodara </h5>-->
    <!--                            <p class="card-text">The station is most notable for its spiral rail line at this location, popularly known as the 'Demodara Loop'. The rail line passes under itself</p>-->
    <!--                        </div>-->
    <!---->
    <!--                    </div>-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/station/JAFFNASTATION1.JPG" style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body ">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Jaffna</h5>-->
    <!--                            <p class="card-text">The popular Yarl Devi service, which operates on the Northern Line station.In the late 1980s the station suffered heavy damage due to the civil war.</p>-->
    <!--                        </div>-->
    <!---->
    <!--                    </div>-->
    <!--                    <div class="card shadow-lg">-->
    <!--                        <img src="../dist/img/station/kansy.jpg" style="height: 200px" class="card-img-top" alt="...">-->
    <!--                        <div class="card-body">-->
    <!--                            <h5 class="card-title" style="font-family: 'Comic Sans MS'">Kandy</h5>-->
    <!--                            <p class="card-text">Kandy railway station is a major railway station in Kandy, Sri Lanka. The station is served by Sri Lanka Railways and is the primary railway station</p>-->
    <!--                        </div>-->
    <!---->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div><div class="row mt-2">-->
    <!---->
    <!---->
    <!---->
    <!--    </section>-->


    <!--==========================
      Footer
    ============================-->
    <footer id="footer">
        <div class="footer-top">
            <div class="container">

            </div>
        </div>

        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong>2019</strong>. All Rights Reserve
            </div>
            <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Regna
                -->
                Designed by
            </div>
        </div>
    </footer><!-- #footer -->

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="../dist/lib/jquery/jquery.min.js"></script>
    <script src="../dist/lib/jquery/jquery-migrate.min.js"></script>
    <script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../dist/lib/easing/easing.min.js"></script>
    <script src="../dist/lib/wow/wow.min.js"></script>
    <script src="../dist/lib/waypoints/waypoints.min.js"></script>
    <script src="../dist/lib/counterup/counterup.min.js"></script>
    <script src="../dist/lib/superfish/hoverIntent.js"></script>
    <script src="../dist/lib/superfish/superfish.min.js"></script>

    <script src="../dist/controller/configuration.js"></script>

    <!-- Contact Form JavaScript File -->
    <script src="../dist/lib/contactform/contactform.js"></script>

    <!-- Template Main Javascript File -->
    <script src="../dist/lib/javaScript/main.js"></script>

    <script src="../dist/lib/javaScript/custom.js" type="text/javascript"></script>


</main>
</body>
</html>
