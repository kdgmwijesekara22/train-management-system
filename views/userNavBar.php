<?php

if(isset($_SESSION['user'])){
    $id = $_SESSION['user'];
}

?>
<nav class="navbar navbar-expand navbar-dark fixed-top" style="background-color: #0069d9;!important;">

    <a id="userNav_UserDashboard" class="navbar-brand mr-1">User Dashboard</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0 bg-transparent">
        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">

                POINT&nbsp;<i class="fas fa-star"></i>
                <span class="badge badge-danger"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right p-0 bg-light" aria-labelledby="alertsDropdown">
                <div class="card" style="width: 30rem">
                    <div class="card-body">
                        <p  class="card-text">User name :
                            <label class="text-success" for="" id="name"></label>
                        </p>
                        <p  class="card-text"> Earned Points
                            &nbsp;
                            <i class="fas fa-star text-warning"></i>
                            <i class="fas fa-star text-warning"></i>
                            <label class="text-success" for="text text-warning" id="point">  </label>
                        </p>
                        <button class="btn btn-warning btn-sm float-right">Read More</button>
                    </div>
                </div>
            </div>
        </li>

        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-circle fa-fw"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right text-center bg-light" aria-labelledby="userDropdown">

                <img class="rounded-circle" src="../dist/img/face-3.jpg" alt="Card image cap">
                <a class="btn btn-primary m-3" href="#"
                   data-toggle="modal" data-target="#logoutModal">Logout</a>
            </div>

        </li>
    </ul>



</nav>
