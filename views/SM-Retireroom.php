<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../dist/lib/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="../dist/lib/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../dist/lib/bootstrap/css/sb-admin.css" rel="stylesheet">


    <style>
        .table {
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
        }

        .table thead th {
            font-weight: bold;
            text-align: center;
        }

        .table tbody td {
            text-align: center;
        }
        .table tbody td{
            color: black;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        /* Dark mode styles */
        .dark-mode {
            background-color: #2e5a87;
            color: #fff;
        }

        /* Sticky sidebar */
        .sidebar {
            position: sticky;
            top: 0;
            height: 100vh;
            overflow-y: auto;
        }

        /* Toggle switch styles */
        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 20px;
            margin-right: 10px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #2e5a87;
            border-radius: 20px;
            transition: 0.4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 16px;
            width: 16px;
            left: 2px;
            bottom: 2px;
            background-color: #ffffff;
            border-radius: 50%;
            transition: 0.4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            transform: translateX(20px);
        }

        /*slide show*/
        .slideshow-container {
            max-width: 100%;
            position: relative;
            margin-bottom: 20px;
        }

        .slide {
            display: none;
            width: 100%;
            height: auto;
        }

    </style>
</head>

<body id="page-top">

<!-- Requiring navBar-->
<?php require_once 'employeNavbar.php'; ?>

<div id="wrapper">

    <!-- Sidebar -->

    <!-- Requiring side Bar panel-->
    <?php require_once 'employeeSideBar.php'; ?>

    <div id="content-wrapper">

        <div class="container-fluid" style="margin-top: 2.9rem">

            <!-- Breadcrumbs-->
            <div class="card mb-3" style="background-color: #2196F3; font-size:larger;">
                <div class="card-header">
                    <i class="fas fa-users-cog" style="font-size: xx-large;"></i>
                    <b style="font-size: xx-large;">Retire-Room Reservations </b>
                </div>
            </div>
            <!-- mode button-->
            <div class="toggle-container">
                <label class="switch">
                    <input type="checkbox" id="darkModeToggle">
                    <span class="slider round"></span>
                </label>
                <span id="modeLabel">Dark Mode</span>
            </div>


            <div class="card" style="background-color: #b8b8e1;">
                <div class="card-body">
                    <div class="row">

                        <!--                       change the appearance by pp-->
                        <div class="card col-md-5" style="background-color: #b8b8e1;">
                            <div class="card-body">
                                <div id="message-scroller" class="card-body p-2"
                                     style="height:70vh;width:100%;overflow:auto;background-color:#d9d9d9;color:white;scrollbar-base-color:#d9d9d9;">
                                    <ul id="messagebody" class="list-group border-0 p-0"
                                        style="background-color:#afafe7;!important;">


                                        <!--                                        second item-->
                                        <li class="list-group-item border-0">

                                            <div class="card shadow-lg">
                                                <img src="../dist/img/retire-rooms/nil.jpg" style="height: 200px"
                                                     class="card-img-top" alt="...">

                                                <div class="card-body text-center">
                                                    <h5 class="card-title text-dark"
                                                        style="font-family: 'Comic Sans MS'"><B>Nil Diya Beach Resort
                                                        </B></h5>
                                                    <p class="text-secondary ">Located in Matara,
                                                        Nildiya beach resort has enchanting restaurants , Magnificent chandeliers, seafaring memorabilia and so on.</p>

                                                </div>
                                                <div class="card-footer text-center">
                                                    <a id="userMainReserveNowCard">
                                                        <button class="btn btn-outline-info m-2" type="button">
                                                            <a href="https://www.google.com/travel/search?q=resort%20in%20matara&g2lb=2502548%2C2503771%2C2503781%2C4258168%2C4270442%2C4284970%2C4291517%2C4597339%2C4757164%2C4814050%2C4850738%2C4864715%2C4874190%2C4886480%2C4893075%2C4924070%2C4965990%2C4990494%2C72262112%2C72285061%2C72286089%2C72298667%2C72300991%2C72302247%2C72305577%2C72309950%2C72314853&hl=en-LK&gl=lk&ssta=1&ts=CAESCAoCCAMKAggDGhwSGhIUCgcI5w8QBxgdEgcI5w8QBxgeGAEyAhAAKgcKBToDTEtS&qs=CAEyE0Nnb0ktYkhBby1lNDBaeDlFQUU4CkILCWVtClsBRiC_GAFCCwn5GHB0xkU5fRgBQgsJPRmZFVomVeoYAUILCe0-PxrKHU9mGAE&ap=MABoAboBCG92ZXJ2aWV3&ictx=1&sa=X&ved=0CAAQ5JsGahcKEwiAtbmqvayAAxUAAAAAHQAAAAAQDA">View more </a>
                                                        </button>
                                                    </a>

                                                </div>
                                            </div>
                                        </li>
                                        <!--                                        first item-->
                                        <li class="list-group-item border-0">
                                            <div class="card shadow-lg">
                                                <img src="../dist/img/retire-rooms/bara.jpg" style="height: 200px"
                                                     class="card-img-top" alt="...">

                                                <div class="card-body text-center">
                                                    <h5 class="card-title text-dark"
                                                        style="font-family: 'Comic Sans MS'">
                                                        Bara Beach - Galle </h5>
                                                    <p class="text-secondary">Located in Galle,
                                                        Offering a restaurant and a private beach area, Free WiFi access is available in this holiday home..</p>
                                                </div>
                                                <div class="card-footer text-center">
                                                    <a id="userMainReserveNowCard">
                                                        <button class="btn btn-outline-info m-2" type="button">
                                                            <a href="https://www.google.com/travel/search?q=resorts%20in%20galle&g2lb=2502548%2C2503771%2C2503781%2C4258168%2C4270442%2C4284970%2C4291517%2C4597339%2C4757164%2C4814050%2C4850738%2C4864715%2C4874190%2C4886480%2C4893075%2C4924070%2C4965990%2C4990494%2C72262112%2C72285061%2C72286089%2C72298667%2C72300991%2C72302247%2C72305577%2C72309950%2C72311291%2C72314853&hl=en-LK&gl=lk&ssta=1&ts=CAESCAoCCAMKAggDGhwSGhIUCgcI5w8QBxgdEgcI5w8QBxgeGAEyAhAA&qs=CAEyE0Nnb0kxOEtGOFpYTzg0MTlFQUU4CkILCVdhIV5xzht9GAFCCwmD7CXevA-SAxgBQgsJYXd6V8IkSf8YAUILCd4rHpfcMWu8GAFaAggB&ap=aAG6AQhvdmVydmlldw&ictx=1&sa=X&sqi=2&ved=0CAAQ5JsGahcKEwiAoPabvKyAAxUAAAAAHQAAAAAQDQ">View more</a>

                                                        </button>
                                                    </a>

                                                </div>
                                            </div>
                                        </li>


                                        <li class="list-group-item border-0">

                                            <div class="card shadow-lg">
                                                <img src="../dist/img/retire-rooms/col.jpg" style="height: 200px"
                                                     class="card-img-top" alt="...">
                                                <div class="card-body text-center">
                                                    <h5 class="card-title text-dark"
                                                        style="font-family: 'Comic Sans MS'">
                                                        Colombo Beach Hotel</h5>
                                                    <p class="text-secondary">Located in Colombo,
                                                        Boutique City Hostel conveniently located in Mount Lavinia, approximately 10km from the city of Colombo </p>

                                                </div>
                                                <div class="card-footer text-center">
                                                    <a id="userMainReserveNowCard">
                                                        <button class="btn btn-outline-info m-2" type="button">
                                                            <a href="https://www.google.com/travel/search?q=colombo%20resorts&g2lb=2502548%2C2503771%2C2503781%2C4258168%2C4270442%2C4284970%2C4291517%2C4597339%2C4757164%2C4814050%2C4850738%2C4864715%2C4874190%2C4886480%2C4893075%2C4924070%2C4965990%2C4990494%2C72262112%2C72285061%2C72286089%2C72298667%2C72300991%2C72302247%2C72305577%2C72309950%2C72314853&hl=en-LK&gl=lk&ssta=1&ts=CAESCAoCCAMKAggDGhwSGhIUCgcI5w8QBxgdEgcI5w8QBxgeGAEyAhAAKgcKBToDTEtS&qs=CAEyE0Nnb0lpZnF6bHUyNjQ5US1FQUU4CkILCVGc1wAX0S-DGAFCCwnAzHiS2ui11RgBQgsJCf3M0taNqT4YAUILCcdbAuyx7hnXGAFaAggB&ap=MABoAboBCG92ZXJ2aWV3&ictx=1&sa=X&ved=0CAAQ5JsGahcKEwiIkMGRvqyAAxUAAAAAHQAAAAAQDA">View More </a>
                                                        </button>
                                                    </a>

                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item border-0">

                                            <div class="card shadow-lg">
                                                <img src="../dist/img/retire-rooms/kandy.jpg" style="height: 200px"
                                                     class="card-img-top" alt="...">
                                                <div class="card-body text-center">
                                                    <h5 class="card-title text-dark"
                                                        style="font-family: 'Comic Sans MS'">Kandy Nature Resort</h5>
                                                    <p class="text-secondary"> Located in Ampitiya, within 2.7 miles of Bogambara Stadium and 2.7 miles of Kandy City Center Shopping Mall</p>
                                                </div>
                                                <div class="card-footer text-center">
                                                    <a id="userMainReserveNowCard">
                                                        <button class="btn btn-outline-info m-2" type="button">
                                                            <a href="https://www.google.com/travel/search?q=kandy%20resorts&g2lb=2502548%2C2503771%2C2503781%2C4258168%2C4270442%2C4284970%2C4291517%2C4597339%2C4757164%2C4814050%2C4850738%2C4864715%2C4874190%2C4886480%2C4893075%2C4924070%2C4965990%2C4990494%2C72262112%2C72285061%2C72286089%2C72298667%2C72300991%2C72302247%2C72305577%2C72309950%2C72314853&hl=en-LK&gl=lk&ssta=1&ts=CAESCAoCCAMKAggDGhwSGhIUCgcI5w8QBxgdEgcI5w8QBxgeGAEyAhAAKgcKBToDTEtS&qs=CAEyFENnc0l4WktKOHBhejE0dTlBUkFCOApCCwmXACycYngF5BgBQgsJRUlCbpldF70YAUILCZYDjb0iFxRmGAFCCwlmoB8UR7eLiRgB&ap=MABoAboBCG92ZXJ2aWV3&ictx=1&sa=X&ved=0CAAQ5JsGahcKEwiAgpWMv6yAAxUAAAAAHQAAAAAQDA">View More</a>

                                                        </button>
                                                    </a>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>


                        <form class="col-md-7">

                            <div class="form-row">


                                <!--                               add slide show-->
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div id="imageSlideshow" class="slideshow-container" style="width: 250px; height: 150px;">
                                            <img class="slide" src="../dist/img/retire-rooms/ret1.jpg" alt="Image 1">
                                            <img class="slide" src="../dist/img/retire-rooms/ret2.jpg" alt="Image 2">
                                            <img class="slide" src="../dist/img/retire-rooms/ret3.jpg" alt="Image 3">
                                            <img class="slide" src="../dist/img/retire-rooms/ret4.jpg" alt="Image 4">
                                            <!-- Add more images here -->
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <select class="custom-select mr-sm-2" id="resorts">
                                        <option selected> Select Retire Room</option>
                                        <option value="Kandy Staion">Kandy Staion</option>
                                        <option value="Polonnaruwa Station">Polonnaruwa Station</option>
                                        <option value="Batticaloa Station">Batticaloa Station</option>
                                        <option value="Anuradhapura Station">Anuradhapura Station</option>
                                        <option value="Mihinthale Station">Mihinthale Station</option>
                                        <option value="Trincomalee Station">Trincomalee Station</option>
                                        <option value="Jaffna Station">Jaffna Station</option>
                                        <option value="Galle Station">Galle Station</option>

                                    </select>
                                    <br>
                                    <hr>



                                </div>



                            </div>

                            <br>
                            <br>

                            <div class="form-row">
                                <div class="col-md-6">
                                    <input id="passenName" type="text" class="form-control" placeholder=" Your Name">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <label for="" style="color: black;">From</label>
                                    <input id="startDate" type="date" class="form-control" placeholder="">
                                </div>
                                <div class="col-md-6">
                                    <label for="" style="color: black;">To</label>
                                    <input id="endDate" type="date" class="form-control" placeholder="">
                                </div>
                            </div>

                            <br>
                            <div class="form-row">
                                <div class="col-md-5">
                                    <input id="mobiNo" type="text" class="form-control" placeholder="Mobile No">
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col-md-5">
                                    <input id="passNic" type="text" class="form-control" placeholder="NIC">
                                </div>
                            </div>

                            <br>
                            <div class="form-row form-control-lg">
                                <div class="col-md-2" id="reserveBtnArea">
                                    <button id="reservResortBtn" type="button" class="btn btn-success float-right "
                                            data-toggle="modal"
                                            data-target="#exampleModal"
                                            data-whatever="@getbootstrap" style="background-color: #00aced;"><i class="fas fa-user-edit"> </i>Reserve
                                    </button>
                                </div>


                            </div>


                            <br>


                        </form>






                    </div>
                </div>
            </div>


        </div>
        <!--     resort reservation pop up-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-dark">
                        <h5 class="modal-title text-white" id="exampleModalLabel">Resort Reservations Summary</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="text-center font-weight-bold">

                                <label for="refNo" class="">Your Reference No:</label>
                                <label id="refNo" for="" class="text-danger">454216</label>
                            </div>
                            <div class="form-row m-3">

                                <label for="selectResort" class="">Selected Resort : </label>
                                <label id="selectResort" for="" class="text-primary"></label>
                            </div>
                            <div class="form-row m-3">

                                <label for="passengerName" class="">Passenger Name : </label>
                                <label id="passengerName" for="" class="text-primary"></label>
                            </div>
                            <div class="form-row m-3">

                                <label for="from" class="">From: </label>
                                <label id="from" for="" class="text-primary col-md-4"></label>
                                <label for="to" class="">To : </label>
                                <label id="to" for="" class="text-primary col-md-4"></label>
                            </div>
                            <div class="form-row m-3">

                                <label for="mobileNo" class="">Mobile No : </label>
                                <label id="mobileNo" for="" class="text-primary"></label>
                            </div>
                            <div class="form-row m-3">

                                <label for="nicNo" class="">NIC : </label>
                                <label id="nicNo" for="" class="text-primary"></label>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="float-left">
                            <button id="" type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                        </div>
                        <div>
                            <button id="submit-Reservation" type="button" class="btn btn-success">Submit</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Sticky Footer -->

        <?php
        require_once 'footer.php';
        ?>
    </div>
    <!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #00aced;font-size: larger;">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Are you sure you want to logout ! </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="">Logout</a>
            </div>
        </div>
    </div>
</div>

<script>
    let userName = "<?php echo $id; ?>";
</script>

<!-- Bootstrap core JavaScript-->
<script src="../dist/lib/jquery/jquery.min.js"></script>
<script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="../dist/lib/jquery-easing/jquery.easing.min.js"></script>
<script src="../dist/controller/configuration.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--<script src="../dist/controller/userProfileController.js"></script>-->
<script src="../dist/controller/employeeretireroomReservationController.js"></script>

<!-- Custom scripts for all pages-->
<script src="../dist/lib/bootstrap/js/sb-admin.min.js"></script>
<script>

    // Function to toggle dark mode
    function toggleDarkMode() {
        const body = document.body;
        body.classList.toggle('dark-mode');
        // Update the label text based on the current mode
        const modeLabel = document.getElementById('modeLabel');
        if (body.classList.contains('dark-mode')) {
            modeLabel.textContent = 'Light Mode';
        } else {
            modeLabel.textContent = 'Dark Mode';
        }
    }

    // Check if the user has a preferred color scheme (dark mode or light mode) set in their browser
    const userPrefersDark = window.matchMedia('(prefers-color-scheme: dark)').matches;

    // Toggle dark mode based on user preference (if they have dark mode enabled in their browser)
    if (userPrefersDark) {
        document.body.classList.add('dark-mode');
        document.getElementById('modeLabel').textContent = 'Light Mode';
    }

    // Add event listener to the dark mode toggle button
    const darkModeToggle = document.getElementById('darkModeToggle');
    darkModeToggle.addEventListener('click', toggleDarkMode);


    // slid show
    let slideIndex = 0;

    function showSlides() {
        let slides = document.getElementsByClassName("slide");
        for (let i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {
            slideIndex = 1;
        }
        slides[slideIndex - 1].style.display = "block";
        setTimeout(showSlides, 1000); // Change slide every 2 seconds
    }

    // Call the showSlides function to start the slideshow
    showSlides();
</script>
</body>

</html>
