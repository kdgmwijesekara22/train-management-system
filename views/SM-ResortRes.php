<div class="card col-md-5" style="background-color: #b8b8e1;">
    <div class="card-body">
        <div id="message-scroller" class="card-body p-2"
             style="height:70vh;width:100%;overflow:auto;background-color:#d9d9d9;color:white;scrollbar-base-color:#d9d9d9;">
            <ul id="messagebody" class="list-group border-0 p-0"
                style="background-color:#d9d9d9;!important;">

                <li class="list-group-item border-0">

                    <div class="card shadow-lg">
                        <img src="../dist/img/hotel/riv.jpg" style="height: 200px"
                             class="card-img-top" alt="...">
                        <div class="card-body text-center">
                            <h5 class="card-title text-dark"
                                style="font-family: 'Comic Sans MS'">
                                Kandy Riverside Villa</h5>
                            <p class="text-secondary">Located on the banks of the longest river in Sri Lanka, Kandy Riverside Villa features an outdoor pool and restaurant.  </p>

                        </div>
                        <div class="card-footer text-center">
                            <a id="userMainReserveNowCard">
                                <button class="btn btn-outline-info m-2" type="button">
                                    <a href="https://www.google.com/search?q=Kandy+Riverside+Villa&tbm=isch&ved=2ahUKEwj-tun01bCAAxXzyKACHQWiBD4Q2-cCegQIABAA&oq=Kandy+Riverside+Villa&gs_lcp=CgNpbWcQAzIHCAAQGBCABDIHCAAQGBCABDIHCAAQGBCABDIHCAAQGBCABDoHCAAQigUQQ1DBDVjBDWCWEGgAcAB4AIABqAGIAf8BkgEDMS4xmAEAoAEBqgELZ3dzLXdpei1pbWfAAQE&sclient=img&ei=TVLDZL7cK_ORg8UPhcSS8AM&bih=707&biw=1536&rlz=1C1KNTJ_enLK1051LK1051">View More</a>
                                </button>
                            </a>

                        </div>
                    </div>
                </li>

                <li class="list-group-item border-0">

                    <div class="card shadow-lg">
                        <img src="../dist/img/hotel/sun.jpg " style="height: 200px"
                             class="card-img-top" alt="...">

                        <div class="card-body text-center">
                            <h5 class="card-title text-dark"
                                style="font-family: 'Comic Sans MS'"><B>Sunset Galle
                                </B></h5>
                            <p class="text-secondary ">Located in Galle and with Bonavista Beach reachable within a few steps, Sunset Hotel Galle has a restaurant, allergy-free rooms, free WiFi and so on</p>

                        </div>
                        <div class="card-footer text-center">
                            <a id="userMainReserveNowCard">
                                <button class="btn btn-outline-info m-2" type="button">
                                    <a href="https://www.google.com/travel/search?q=hotels%20in%20galle&g2lb=2502548%2C2503771%2C2503781%2C4258168%2C4270442%2C4284970%2C4291517%2C4597339%2C4757164%2C4814050%2C4850738%2C4864715%2C4874190%2C4886480%2C4893075%2C4924070%2C4965990%2C4990494%2C72262112%2C72285061%2C72286089%2C72298667%2C72300991%2C72302247%2C72305577%2C72309950%2C72314853&hl=en-LK&gl=lk&ssta=1&ts=CAESCAoCCAMKAggDGhwSGhIUCgcI5w8QBxgdEgcI5w8QBxgeGAEyAhAAKgcKBToDTEtS&qs=CAEyFENnc0k0ZTdwdTZXWXlhVF9BUkFCOApCCwlhd3pXwiRJ_xgBQgsJg-wl3rwPkgMYAUILCe6kfDNeo_dIGAFCCwnVmEUMVfgzXBgB&ap=MABoAboBCG92ZXJ2aWV3&ictx=1&sa=X&ved=0CAAQ5JsGahcKEwjoy-emhq-AAxUAAAAAHQAAAAAQDA">View More</a>
                                </button>
                            </a>

                        </div>
                    </div>
                </li>
                <li class="list-group-item border-0">
                    <div class="card shadow-lg">
                        <img src="../dist/img/hotel/de.jpg" style="height: 200px"
                             class="card-img-top" alt="...">

                        <div class="card-body text-center">
                            <h5 class="card-title text-dark"
                                style="font-family: 'Comic Sans MS'">
                                de MODA Boutiques Hotel</h5>
                            <p class="text-secondary">de MODA Boutique Hotel Galle has air-conditioned rooms with free WiFi, free private parking and room service.</p>
                        </div>
                        <div class="card-footer text-center">
                            <a id="userMainReserveNowCard">
                                <button class="btn btn-outline-info m-2" type="button">
                                    <a href="https://www.google.com/travel/search?q=hotels%20in%20galle&g2lb=2502548%2C2503771%2C2503781%2C4258168%2C4270442%2C4284970%2C4291517%2C4597339%2C4757164%2C4814050%2C4850738%2C4864715%2C4874190%2C4886480%2C4893075%2C4924070%2C4965990%2C4990494%2C72262112%2C72285061%2C72286089%2C72298667%2C72300991%2C72302247%2C72305577%2C72309950%2C72314853&hl=en-LK&gl=lk&ssta=1&ts=CAESCAoCCAMKAggDGhwSGhIUCgcI5w8QBxgdEgcI5w8QBxgeGAEyAhAAKgcKBToDTEtS&qs=CAEyE0Nnb0k3c255bS1QcjZQdElFQUU4CkILCWF3elfCJEn_GAFCCwmD7CXevA-SAxgBQgsJ7qR8M16j90gYAUILCdWYRQxV-DNcGAE&ap=MABoAboBCG92ZXJ2aWV3&ictx=1&sa=X&ved=0CAAQ5JsGahcKEwiAppPyha-AAxUAAAAAHQAAAAAQBg">View More</a>
                                </button>
                            </a>

                        </div>
                    </div>
                </li>

                <li class="list-group-item border-0">

                    <div class="card shadow-lg">
                        <img src="../dist/img/hotel/ann.jpg" style="height: 200px"
                             class="card-img-top" alt="...">
                        <div class="card-body text-center">
                            <h5 class="card-title text-dark"
                                style="font-family: 'Comic Sans MS'">Anantara Peace Haven Tangalle Resort
                            </h5>
                            <p class="text-secondary">Anantara Peace Haven Tangalle Resort sits on a rocky outcrop on the southern coast of Sri Lanka near the old fort town of Tangalle..</p>
                        </div>
                        <div class="card-footer text-center">
                            <a id="userMainReserveNowCard">
                                <button class="btn btn-outline-info m-2" type="button">
                                    <a href="https://www.google.com/travel/search?q=resorts%20in%20tangalle&g2lb=2502548%2C2503771%2C2503781%2C4258168%2C4270442%2C4284970%2C4291517%2C4597339%2C4757164%2C4814050%2C4850738%2C4864715%2C4874190%2C4886480%2C4893075%2C4924070%2C4965990%2C4990494%2C72262112%2C72285061%2C72286089%2C72298667%2C72300991%2C72302247%2C72305577%2C72314853&hl=en-LK&gl=lk&ssta=1&ts=CAESCAoCCAMKAggDGhwSGhIUCgcI5w8QBxgdEgcI5w8QBxgeGAEyAhAAKgcKBToDTEtS&qs=CAEyE0Nnb0l0dmFuLXVMbjdkQV9FQUU4CkILCbo-uqQY29S4GAFCCwlEhJ-pMRwFtRgBQgsJHpmu2TpAv_kYAUILCTb7SS8-t6E_GAE&ap=KigKEgm1VfZQUAkYQBH98_4vDjJUQBISCcJuOs5WEBhAEf3z_qpHMlRAMABoAboBCG92ZXJ2aWV3&ictx=1&sa=X&ved=0CAAQ5JsGahcKEwiInoDH1bCAAxUAAAAAHQAAAAAQCg">View More</a>
                                </button>
                            </a>

                        </div>
                    </div>
                </li>
            </ul>
        </div>


    </div>

</div>

