<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>


    <style>
        .table {
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
        }

        .table thead th {
            font-weight: bold;
            text-align: center;
        }

        .table tbody td {
            text-align: center;
        }
        .table tbody td{
            color: black;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        /* Dark mode styles */
        .dark-mode {
            background-color: #2e5a87;
            color: #fff;
        }

        /* Sticky sidebar */
        .sidebar {
            position: sticky;
            top: 0;
            height: 100vh;
            overflow-y: auto;
        }

        /* Toggle switch styles */
        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 20px;
            margin-right: 10px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #2e5a87;
            border-radius: 20px;
            transition: 0.4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 16px;
            width: 16px;
            left: 2px;
            bottom: 2px;
            background-color: #ffffff;
            border-radius: 50%;
            transition: 0.4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            transform: translateX(20px);
        }
        thead.thead-dark {
            position: sticky;
            top: 0;
            z-index: 1;
            background-color: #343a40; /* You can adjust the color as per your design */
            color: white;
        }

    </style>
</head>
<body>

<hr>
<!--mode button-->
<div class="toggle-container">
    <label class="switch">
        <input type="checkbox" id="darkModeToggle">
        <span class="slider round"></span>
    </label>
    <span id="modeLabel">Dark Mode</span>
</div>


<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card" style="background-color: lightblue;">
                <div class="card-body">
                    <h3 style="color: black;">SEAT RESERVATION DETAILS</h3>
                    <div id="message-scroller" class="card-body p-3"
                         style="height: 70vh; overflow: auto; background-color: white; color: white; scrollbar-base-color: #d9d9d9;">
                        <table class="table table-hover mb-0">
                            <thead class="thead-dark">
                            <tr>
                                <th class="text-center">RID</th>
                                <th class="text-center">Train Name</th>
                                <th class="text-center">Reserved Date</th>
                                <th class="text-center">From</th>
                                <th class="text-center">TO</th>
                                <th class="text-center">Compartment</th>
                                <th class="text-center">Seat N0</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody id="seatReservationTable">
                            <!-- Your table rows with data go here -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<hr>
<br>



<div class="card col-md-12" style="background-color: #b8b8e1;">
    <div class="card-body">
        <h3 style="color: black;">COMPARTMENT RESERVATION DETAILS</h3>
        <div id="message-scroller" class="card-body p-3"
             style="height: 70vh; overflow: auto; background-color:white; color: white; scrollbar-base-color: #d9d9d9;">

            <table class="table table-hover mb-0">
                <thead class="thead-dark">
                <tr>
                    <th class="text-center">RID</th>
                    <th class="text-center">Train Name</th>
                    <th class="text-center">Reserved Date</th>
                    <th class="text-center">From</th>
                    <th class="text-center">TO</th>
                    <th class="text-center">Compartment</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Action</th>
                    <!--            <th class="text-center">Rate Us</th> -->
                </tr>
                </thead>
                <tbody id="seatCompartmentTable">
                <!-- Your table rows with data go here -->
                </tbody>
            </table>
        </div>
    </div>
</div><hr>
<br>

<!--train reservatio details-->
<div class="card col-md-12" style="background-color: lightgray;">
    <div class="card-body">
        <h3 style="color: black;">TRAIN RESERVATION DETAILS</h3>
        <div id="message-scroller" class="card-body p-3"
             style="height: 70vh; overflow: auto; background-color: white; color: white; scrollbar-base-color: #d9d9d9;">

            <table class="table table-hover mb-0">
                <thead class="thead-dark">
                <tr>
                    <th class="text-center">RID</th>
                    <th class="text-center">Train Name</th>
                    <th class="text-center">Reserved Date</th>
                    <th class="text-center">From</th>
                    <th class="text-center">TO</th>
                    <th class="text-center">Class</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Action</th>
                    <!--            <th class="text-center">Rate Us</th> -->
                </tr>
                </thead>
                <tbody id="trainReservationTable">
                <!-- Your table rows with data go here -->
                </tbody>
            </table>
        </div>
    </div>
</div>




<div class="card-footer small text-muted"></div>


<script>
    // Function to toggle dark mode
    function toggleDarkMode() {
        const body = document.body;
        body.classList.toggle('dark-mode');
        // Update the label text based on the current mode
        const modeLabel = document.getElementById('modeLabel');
        if (body.classList.contains('dark-mode')) {
            modeLabel.textContent = 'Light Mode';
        } else {
            modeLabel.textContent = 'Dark Mode';
        }
    }

    // Check if the user has a preferred color scheme (dark mode or light mode) set in their browser
    const userPrefersDark = window.matchMedia('(prefers-color-scheme: dark)').matches;

    // Toggle dark mode based on user preference (if they have dark mode enabled in their browser)
    if (userPrefersDark) {
        document.body.classList.add('dark-mode');
        document.getElementById('modeLabel').textContent = 'Light Mode';
    }

    // Add event listener to the dark mode toggle button
    const darkModeToggle = document.getElementById('darkModeToggle');
    darkModeToggle.addEventListener('click', toggleDarkMode);
</script>
</body>
</html>