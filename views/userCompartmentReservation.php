<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User - Dashboard</title>
    <!-- Favicons -->
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">
    <!--    <link href="../dist/img/apple-touch-icon.png" rel="apple-touch-icon">-->

    <!-- Bootstrap core CSS-->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Page level plugin CSS-->
    <link href="../dist/lib/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../dist/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../dist/lib/animate/animate.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../dist/lib/css/core.min.css">
    <link href="../dist/lib/css/style.css" rel="stylesheet">
    <link href="../dist/lib/bootstrap/css/sb-admin.min.css" rel="stylesheet">
    <link href="../dist/lib/css/seatBookStyle.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Requiring navBar-->
<?php require_once 'userNavBar.php'; ?>

<div id="wrapper">

    <!-- Sidebar -->

    <!-- Requiring side Bar panel-->
    <?php require_once 'userSideBar.php'; ?>

    <div id="content-wrapper">

        <div class="container-fluid" style="margin-top: 2.9rem">

            <!-- Page Content -->
            <div class="card mb-3" style="background-color: lightskyblue">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                   Compartment Reservation
                </div>
            </div>
            <section id="about">
                <div class="container">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <h3>Reserve a Compartment in long distance train across Sri Lanka</h3>
                            </div>
                            <div class="row">

                                <div class="col-md-4 mb-3">
                                    <label for="resturantName">Starting From</label>

                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fas fa-search-location"
                                                                          data-toggle="tooltip" data-placement="top"
                                                                          title="Press enter after enter the city..!"></i></span>
                                        <input id="startCity" type="text" class="form-control "
                                               name="resturantName"
                                               placeholder="Type city" data-toggle="tooltip" data-placement="top"
                                               title="Press enter after enter the city..!" required>

                                    </div>
                                    <div class="input-group-append mt-1">
                                        <label class="text-danger ">**Main Station : </label>
                                        <label class="ml-2" id="mainStation1"></label>
                                    </div>
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="resturantName">Going To</label>

                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fas fa-search-location"
                                                                          data-toggle="tooltip" data-placement="top"
                                                                          title="Press enter after enter the city..!"></i></span>
                                        <input type="text" class="form-control " id="destination"
                                               name="resturantName"
                                               placeholder="Type your destination" data-toggle="tooltip"
                                               data-placement="top" title="Press enter after enter the city..!"
                                               required>

                                    </div>
                                    <div class="input-group-append mt-1">
                                        <label class="text-danger ">**Main Station : </label>
                                        <label class="ml-2" id="mainStation2"></label>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="resturantName">Journey Date</label>

                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        <input type="date" class="form-control " id="reserveDate"
                                               name="resturantName"
                                               placeholder="Type your destination" required>

                                    </div>

                                </div>

                                <div class="row align-content-center">
                                    <div class="container">
                                        <button class="btn btn-primary m-2" id="userSearch" type="button">Search Train
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <!-- DataTables Example -->
            <div class="container">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-center bg-secondary text-white">Train Name</th>
                        <th class="text-center bg-secondary text-white">From</th>
                        <th class="text-center bg-secondary text-white">TO</th>
                        <th class="text-center bg-secondary text-white">Start Time</th>
                        <th class="text-center bg-secondary text-white">End Time</th>
                        <th class="text-center bg-secondary text-white">Action</th>
                    </tr>
                    </thead>
                    <tbody id="compartmentTable">

                    </tbody>
                </table>
            </div>
            <!-- /.container-fluid -->

            <!-- Sticky Footer -->
        </div


    </div>

    <?php
    require_once 'footer.php';
    ?>
    <!-- /.content-wrapper -->

</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true" style="min-width: 1366px">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
<!--            <div class="modal-header">-->
<!--                <h5 class="modal-title" id="exampleModalCenterTitle">Choose Your Compartment</h5>-->
<!--                <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--                    <span aria-hidden="true">&times;</span>-->
<!--                </button>-->
<!--            </div>-->
            <div class="modal-body">
                <div class="card p-0">
                    <div class="card-body justify-content-between">
                        <div id="seatSelectPage">
                            <h5> Choose compartment by your preference:</h5>
                            <div class="row">
                                <div class="col-2 mt-5">
                                    <ul style="list-style: none; padding-left: 0px">
                                        <li>
                                            <div id="com1" class="text-center comp1">1 compartment</div>
                                        </li>
                                        <li>
                                            <div id="com2" class="text-center comp1">2 compartment</div>
                                        </li>
                                        <li>
                                            <div id="com3" class="text-center comp1">3 compartment</div>
                                        </li>
                                        <li>
                                            <div id="com4" class="text-center comp1">4 compartment</div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-8 text-center mt-3 ml-5 mb-0 mr-0" id="holder1">
                                    <img class="w-100 h-auto" src="../dist/img/compartment/1st%20class.jpg" alt="">
                                </div>

                                <div class="col-8 text-center mt-3 ml-5 mb-0 mr-0" id="holder2">
                                    <img class="w-100 h-auto" src="../dist/img/compartment/1st%20clz.jpg" alt="">

                                </div>
                                <div class="col-8 text-center mt-3 ml-5 mb-0 mr-0" id="holder3">
                                    <img class="w-100 h-auto" src="../dist/img/compartment/1st%20class.jpg" alt="">

                                </div>
                                <div class="col-8 text-center mt-3 ml-5 mb-0 mr-0" id="holder4">
                                    <img class="w-100 h-auto" src="../dist/img/compartment/SLGR-first-class-Carriage.jpg" alt="">
                                </div>
                            </div>
                            <!--                           -->
                            <!--                            <div style="float:left; margin-top: 25px;margin-right: 120px;">-->
                            <!--                                <ul id="seatDescription">-->
                            <!--                                    <li style="background:url('../dist/img/seatbooking/available_seat_img.gif') no-repeat scroll 0 0 transparent;">-->
                            <!--                                        Available-->
                            <!--                                    </li>-->
                            <!--                                    <li style="background:url('../dist/img/seatbooking/booked_seat_img.gif') no-repeat scroll 0 0 transparent;">-->
                            <!--                                        Booked-->
                            <!--                                    </li>-->
                            <!--                                    <li style="background:url('../dist/img/seatbooking/selected_seat_img.gif') no-repeat scroll 0 0 transparent;">-->
                            <!--                                        Selected-->
                            <!--                                    </li>-->
                            <!--                                </ul>-->
                            <!--                            </div>-->
                            <!--                            <br>-->
                            <!--                            <div class="row border-info">-->
                            <!--                                <div style="clear:both;width:100%">-->
                            <!--                                    <button class="btn btn-primary" type="button" id="btnShowNew" value="">-->
                            <!--                                        Selected-->
                            <!--                                        Seats-->
                            <!--                                    </button>-->
                            <!--                                    <button class="btn btn-dark" type="button" id="btnShow" value="Show All">-->
                            <!--                                        Show All-->
                            <!--                                        Seats-->
                            <!--                                    </button>-->
                            <!---->
                            <!---->
                            <!--                                </div>-->
                            <!--                            </div>-->

                            <!--                        </div>-->
                            <!--                    </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <label for="">Compartment No:</label>
                <input class="form-control-sm text-center text-primary" type="text" id="selectedCompartmentNo" readonly>
<!--                <label for="">Amount :</label>-->
<!--                <input class="form-control-sm" type="text">-->
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <button type="button" class="btn btn-primary" data-toggle="modal" data-dismiss="modal"
                        data-target="#exampleModal" data-whatever="@mdo">Book Now
                </button>


            </div>
        </div>
    </div>
</div>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>-->
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="logout">Logout</a>
            </div>
        </div>
    </div>
</div>



<!--        send Request Form-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row text-center ml-1">
                    <label class="form-control col-4">Number Of Seats</label>
                    <input type="text" class="form-control col-2" id="numberOfSeat" name="resturantName">
                    <label class="form-control col-2 ml-1">Amount</label>
                    <input type="text" class="form-control col-3" id="amount" name="amount">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="send-request" type="button" class="btn btn-primary">Send Request</button>
            </div>
        </div>
    </div>
</div>

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>


<script>
    let userName = "<?php echo $id ?>";
</script>

<!-- Bootstrap core JavaScript-->
<script src="../dist/lib/jquery/jquery.min.js"></script>
<script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../dist/controller/configuration.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"
        integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i"
        crossorigin="anonymous"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="../dist/controller/userReservations.js"></script>
<script src="../dist/controller/userCompartmentController.js"></script>
<script src="../dist/controller/passengerController.js"></script>
<script src="../dist/controller/userProfileController.js"></script>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>

</body>

</html>
