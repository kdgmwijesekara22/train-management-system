
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <style>
        .table {
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
        }

        .table thead th {
            font-weight: bold;
            text-align: center;
        }

        .table tbody td {
            text-align: center;
        }
        .table tbody td{
            color: black;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        /* Dark mode styles */
        .dark-mode {
            background-color: #2e5a87;
            color: #fff;
        }

        /* Sticky sidebar */
        .sidebar {
            position: sticky;
            top: 0;
            height: 100vh;
            overflow-y: auto;
        }

        /* Toggle switch styles */
        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 20px;
            margin-right: 10px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #2e5a87;
            border-radius: 20px;
            transition: 0.4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 16px;
            width: 16px;
            left: 2px;
            bottom: 2px;
            background-color: #ffffff;
            border-radius: 50%;
            transition: 0.4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            transform: translateX(20px);
        }

    </style>

    <title>Admin - Dashboard</title>
    <!-- Favicons -->
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">

    <!-- Bootstrap core CSS-->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
          crossorigin="anonymous">

    <!-- Page level plugin CSS-->
    <link href="../dist/lib/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../dist/lib/bootstrap/css/sb-admin.min.css" rel="stylesheet">
    <link href="../dist/lib/css/style.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Requiring navBar-->
<?php require_once 'navBar.php'; ?>

<div id="wrapper" class="d-flex">

    <!-- Sidebar -->
    <nav class="sidebar bg-light">
        <!-- Requiring side Bar panel-->
        <?php require_once 'sideBar.php'; ?>
    </nav>

    <div id="content-wrapper" class="container-fluid">

        <div class="container-fluid" style="margin-top: 2.9rem">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a>Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Overview</li>
            </ol>


            <div class="row mb-3">
                <div class="col-md-4">
                    <div class="card bg-primary text-white">
                        <div class="card-body">
                            <h5 class="card-title">Total Trains</h5>
                            <p class="card-text">Explore the number of trains in the system.</p>
                            <div class="card-summary">Summary: <span class="card-count">50</span></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-info text-white">
                        <div class="card-body">
                            <h5 class="card-title">Total Requests</h5>
                            <p class="card-text">Review the total number of pending requests.</p>
                            <div class="card-summary">Summary: <span class="card-count">24</span></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-primary text-white">
                        <div class="card-body">
                            <h5 class="card-title">Total Users</h5>
                            <p class="card-text">Manage the registered users in the system.</p>
                            <div class="card-summary">Summary: <span class="card-count">878</span></div>
                        </div>
                    </div>
                </div>
            </div>

            <h4 style="color: black;">Train Schedule 2023</h4>
            <hr>

            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table" style="color: black;"></i><b style="color: #1b1e21"> Train Schedule 2023</b>

                </div>
            </div>


            <div class="toggle-container">
                <label class="switch">
                    <input type="checkbox" id="darkModeToggle">
                    <span class="slider round"></span>
                </label>
                <span id="modeLabel">Dark Mode</span>
            </div>


            <div class="container">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-center bg-secondary text-white">Train Name</th>
                        <th class="text-center bg-secondary text-white">From</th>
                        <th class="text-center bg-secondary text-white">To</th>
                        <th class="text-center bg-secondary text-white">Date</th>
                        <th class="text-center bg-secondary text-white">Start Time</th>
                        <th class="text-center bg-secondary text-white">End Time</th>
                    </tr>
                    </thead>
                    <tbody id="scheduleTable"  style="color: black;">

                    </tbody>
                </table>
            </div>

        </div>
    </div>




</div>
<!-- /.content-wrapper -->

<?php require_once 'footer.php'; ?>

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>-->
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="logout">Logout</a>
            </div>
        </div>
    </div>
</div>


<footer class="sticky-footer" style="font-family: 'Numans', sans-serif; color: black; font-size: small;">
    <div class="container my-auto">
        <div class="row justify-content-center">
            <div class="col-md-3 text-center">
                <a href="#">
                    <i class="fas fa-train fa-2x"></i>
                    <span>National Railway Cooperation - Sri Lanka</span>
                </a>
            </div>
            <div class="col-md-3 text-center">
                <a href="tel:011-9478123">
                    <i class="fas fa-phone fa-2x"></i>
                    <span>011-9478123</span>
                </a>
            </div>
            <div class="col-md-3 text-center">
                <a href="mailto:NationalRailway@gmail.com">
                    <i class="fas fa-envelope fa-2x"></i>
                    <span>NationalRailway@gmail.com</span>
                </a>
            </div>
            <div class="col-md-3 text-center">
                <a href="https://www.NationalRailway.com" target="_blank">
                    <i class="fas fa-map-marker-alt fa-2x"></i>
                    <span>http://www.railway.gov.lk/</span>
                </a>
            </div>
        </div>
    </div>
</footer>

<!-- Bootstrap core JavaScript-->
<script src="../dist/lib/jquery/jquery.min.js"></script>
<script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../dist/controller/configuration.js"></script>

<script>
    // Function to toggle dark mode
    function toggleDarkMode() {
        const body = document.body;
        body.classList.toggle('dark-mode');
        // Update the label text based on the current mode
        const modeLabel = document.getElementById('modeLabel');
        if (body.classList.contains('dark-mode')) {
            modeLabel.textContent = 'Light Mode';
        } else {
            modeLabel.textContent = 'Dark Mode';
        }
    }

    // Check if the user has a preferred color scheme (dark mode or light mode) set in their browser
    const userPrefersDark = window.matchMedia('(prefers-color-scheme: dark)').matches;

    // Toggle dark mode based on user preference (if they have dark mode enabled in their browser)
    if (userPrefersDark) {
        document.body.classList.add('dark-mode');
        document.getElementById('modeLabel').textContent = 'Light Mode';
    }

    // Add event listener to the dark mode toggle button
    const darkModeToggle = document.getElementById('darkModeToggle');
    darkModeToggle.addEventListener('click', toggleDarkMode);
</script>

</script>
<script src="../dist/controller/scheduleController.js"></script>

</body>

</html>