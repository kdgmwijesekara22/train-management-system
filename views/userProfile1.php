<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User - Dashboard</title>
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">
    <!--    <link href="../dist/img/apple-touch-icon.png" rel="apple-touch-icon">-->

    <!-- Bootstrap core CSS-->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Page level plugin CSS-->
    <link href="../dist/lib/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../dist/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../dist/lib/animate/animate.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../dist/lib/css/core.min.css">
    <link href="../dist/lib/css/style.css" rel="stylesheet">
    <link href="../dist/lib/bootstrap/css/sb-admin.min.css" rel="stylesheet">
    <link href="../dist/lib/css/seatBookStyle.css" rel="stylesheet">

</head>
<body>
<?php require_once 'userNavBar.php'; ?>
<div id="wrapper">
    <?php require_once 'userSideBar.php'; ?>
    <div id="content-wrapper">
        <div class="container-fluid" style="margin-top: 2.9rem">
            <div class="card mb-3" style="background-color: lightskyblue">
                <div class="card-header">
                    <i class="fas fa-users-cog"></i>
                    My Profile
                </div>
            </div>
            <section id="about">
                <div class="row">
                    <div class="container">
                        <div id="login-card" class="position-absolute m-auto card m-3 p-0 shadow-lg"
                             style="top: 0;bottom: 0;right: 0;left: 0;width: 50vw;height: fit-content!important;">
                            <div class="card-header">
                                <label id="title-font">
                                    <i class="fas fa-users-cog"></i>
                                    User Profile
                                </label>
                            </div>
                            <div class="card-body">
                                <form>
                                    <div class="row">
                                        <label for="userNameProfile" class="col-sm-3 col-form-label">User Name
                                            : </label>
                                        <label id="userNameProfile" for="" class="col-sm-8 col-form-label"></label>
                                    </div>
                                    <div class="row">
                                        <label for="userEmailProfile" class="col-sm-3 col-form-label">E-mail : </label>
                                        <label id="userEmailProfile" for="" class="col-sm-8 col-form-label"></label>
                                    </div>
                                    <div class="row">
                                        <label for="userNicProfile" class="col-sm-3 col-form-label">NIC : </label>
                                        <label id="userNicProfile" for="" class="col-sm-4 col-form-label"></label>
                                    </div>
                                    <div class="row">
                                        <label for="userMobileProfile" class="col-sm-3 col-form-label">Mobile : </label>
                                        <label id="userMobileProfile" for="" class="col-sm-4 col-form-label"></label>
                                    </div>

                                    <div class="row">
                                        <label for="userPointsProfile" class="col-sm-3 col-form-label">Loyality Points : </label>
                                        <label id="userPointsProfile" for="" class="col-sm-8 col-form-label"></label>
                                    </div>

                                    <div class="form-row row my-3 center">

                                        <button class="btn btn-success" type="button" data-toggle="modal"
                                                data-target="#editDetails"
                                                data-whatever="@getbootstrap">
                                            Edit Profile Details
                                        </button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!--edit details model-->
<div class="modal fade" id="editDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#808080;">
                <h5 class="modal-title text-white" id="exampleModalLabel">My Profile Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-row">
                        <div class="col-6">
                            <input id="modalUsername" type="text" class="form-control" placeholder="User Name"
                                   style="margin-bottom:10px;"
                                   data-toggle="tooltip" title="User Name"
                                   value="<?php echo $_SESSION['user']?>"
                                   readonly>
                        </div>
                    </div>
                    <br>
                    <div class="form-row">
                        <div class="col-6">
                            <input id="modalFirstName" type="text" class="form-control" placeholder="First Name"
                                   data-toggle="tooltip" title="First Name"
                            >
                        </div>
                        <div class="col-6">
                            <input id="modalLastName" type="text" class="form-control" placeholder="Last Name"
                                   data-toggle="tooltip" title="Last Name"
                            >
                        </div>
                    </div>
                    <br>
                    <div class="form-row">
                        <div class="col-12">
                            <input id="modalEmail" type="email" class="form-control" placeholder="email"
                                   data-toggle="tooltip" title="E-mail"
                                   value=""
                            >
                        </div>
                    </div>
                    <br>
                    <div class="form-row">
                        <div class="col-6">
                            <input id="modalPhoneNumber" type="text" class="form-control" placeholder="Mobile No"
                                   data-toggle="tooltip" title="Mobile No" value=""
                            >
                        </div>
                        <div class="col-6">
                            <input id="modalNic" type="text" class="form-control" placeholder="NIC"
                                   data-toggle="tooltip" title="NIC NO" value=""
                            >
                        </div>
                    </div>
                    <br>

                    <br>

                    <div class="form-row">
                        <div class="col-6">
                            <input id="password" type="password" class="form-control" placeholder="Password"
                                   data-toggle="tooltip" title="New Password"
                            >
                        </div>
                        <div class="col-6">
                            <input id="modalconfirmpassword" type="password" class="form-control"
                                   placeholder="New Password"
                                   data-toggle="tooltip" title="Confirm-password">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color:#808080">
                <button type="button" class="btn btn-secondary float-left" data-dismiss="modal">Close</button>
                <button id="updateModalPassenger" type="button" class="btn btn-dark">Update Detail</button>
            </div>
        </div>
    </div>
</div>

<?php
require_once 'footer.php';
?>
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>-->
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="logout">Logout</a>
            </div>
        </div>
    </div>
</div>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<script>
    let userName = "<?php echo $id ?>";
</script>

<!-- Bootstrap core JavaScript-->
<script src="../dist/lib/jquery/jquery.min.js"></script>
<script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"
        integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i"
        crossorigin="anonymous"></script>
<script src="../dist/controller/configuration.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../dist/controller/userProfileController.js"></script>
<script src="../dist/controller/passengerController.js"></script>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>

</body>

</html>
