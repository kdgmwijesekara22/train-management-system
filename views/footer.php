
<!--<footer class="sticky-footer" >-->
<!--<div class="container my-auto">-->
<!--    <div class="copyright text-center my-auto">-->
<!--        <span> Copyright ©  </span>-->
<!--    </div>-->
<!--</div>-->
<!--</footer >-->

<footer class="sticky-footer" style="font-family: 'Numans', sans-serif; color: black; font-size: 12px;">
    <div class="container my-auto">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-6 col-sm-12 text-center mb-3">
                <a href="#">
                    <i class="fas fa-train fa-2x"></i>
                    <span>National Railway Cooperation - Sri Lanka</span>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 text-center mb-3">
                <a href="tel:011-9478123">
                    <i class="fas fa-phone fa-2x"></i>
                    <span>011-9478123</span>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 text-center mb-3">
                <a href="mailto:NationalRailway@gmail.com">
                    <i class="fas fa-envelope fa-2x"></i>
                    <span>NationalRailway@gmail.com</span>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 text-center mb-3">
                <a href="https://www.NationalRailway.com" target="_blank">
                    <i class="fas fa-map-marker-alt fa-2x"></i>
                    <span>http://www.railway.gov.lk/</span>
                </a>
            </div>
        </div>
    </div>
</footer>



