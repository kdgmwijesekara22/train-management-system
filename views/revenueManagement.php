<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Dashboard</title>
    <!-- Favicons -->
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">


    <!-- Bootstrap core CSS-->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Page level plugin CSS-->
    <link href="../dist/lib/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../dist/lib/bootstrap/css/sb-admin.min.css" rel="stylesheet">
    <link href="../dist/lib/css/style.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Requiring navBar-->
<?php require_once 'navBar.php'; ?>

<div id="wrapper">

    <!-- Sidebar -->

    <!-- Requiring side Bar panel-->
    <?php require_once 'sideBar.php'; ?>

    <div id="content-wrapper">

        <div class="container-fluid" style="margin-top: 2.9rem">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a id="admin-Dash">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Manage Revenue</li>
            </ol>
            <h4 class="text-secondary">Manage Revenue</h4>
            <hr>

            <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Revenue Details Summary
                    <button type="button" class="btn btn-success float-right" data-toggle="modal"
                            data-target="#exampleModal"
                            data-whatever="@getbootstrap"><i class="fas fa-user-edit"> </i>Add New
                    </button>
                </div>
            </div>
            <div class="container">
                <table class="table table-hover">

                    <thead>
                    <tr>
                        <th class="text-center bg-secondary text-white">Type</th>
                        <th class="text-center bg-secondary text-white">Discount</th>
                        <th class="text-center bg-secondary text-white">Date</th>
                        <th class="text-center bg-secondary text-white">Action</th>
                    </tr>
                    </thead>
                    <tbody id="userTable">

                    <tr>
                        <td class="text-center">Seat</td>
                        <td class="text-center">20%</td>
                        <td class="text-center">2023-08-10</td>
                        <td class="text-center">
                            <button class="btn btn-warning" data-toggle="modal" data-target="#exampleModal">Edit</button>
                            <button class="btn btn-danger">Delete</button>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">Compartment</td>
                        <td class="text-center">10%</td>
                        <td class="text-center">2023-08-11</td>
                        <td class="text-center">
                            <button class="btn btn-warning">Edit</button>
                            <button class="btn btn-danger">Delete</button>
                        </td>
                    </tr>

                    </tbody>

                </table>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <!-- Sticky Footer -->
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#808080">
                <h5 class="modal-title text-white" id="exampleModalLabel">Revenue Management</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-row">
                        <div class="col-6">
                            <label  for="discount"> Seat</label>
                            <select class="form-control " name="">
                                <option value="Seat">Seat</option>
                                <option value="Compartment">Compartment</option>
                                <option value="Train">Train</option>
                                <option value="Resort">Resort</option>
                                <option value="RetireRoom">Retire Room</option>
                            </select></div>
                        <div class="col-6">
                            <label  for="discount"> Discount</label>
                            <input name="discount" id="discount" type="text" class="form-control" placeholder="Discount">
                        </div>
                        <br>
                        <div class="col-6 mb-3">
                            <label for="rdate"> Date</label>
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                <input type="date" class="form-control " id="date"
                                       name="rdate"
                                       placeholder="Type your destination" required>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="float-left">
                    <button id="clearBTN" type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                </div>
                <div>
                    <button id="createPassengerAccount" type="button" class="btn btn-success">Edit</button>
                </div>

            </div>
        </div>
    </div>
</div>


<!--edit details model-->
<div class="modal fade" id="editDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#808080">
                <h5 class="modal-title text-white" id="exampleModalLabel">Revenue Management</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-row">
                        <div class="col-6">
                            <label  for="discount"> Seat</label>
                            <select class="form-control " name="">
                                <option value="Seat">Seat</option>
                                <option value="Compartment">Compartment</option>
                                <option value="Train">Train</option>
                                <option value="Resort">Resort</option>
                                <option value="RetireRoom">Retire Room</option>
                            </select></div>
                        <div class="col-6">
                            <label  for="discount"> Discount</label>
                            <input name="discount" id="discount" type="text" class="form-control" placeholder="Discount">
                        </div>
                        <br>
                        <div class="col-6 mb-3">
                            <label for="rdate"> Date</label>
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                <input type="date" class="form-control " id="date"
                                       name="rdate"
                                       placeholder="Type your destination" required>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary float-left" data-dismiss="modal">Close</button>
                <button type="button" id="updateEmp" class="btn btn-dark">Update Detail</button>

            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php'; ?>

<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>-->
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="logout">Logout</a>
            </div>
        </div>
    </div>
</div>
<!-- Bootstrap core JavaScript-->
<script src="../dist/lib/jquery/jquery.min.js"></script>
<script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>


<!-- Demo scripts for this page-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../dist/controller/configuration.js"></script>
<script src="../dist/controller/manageRevenueController.js.js"></script>
<!--<script src="../dist/controller/passengerController.js"></script>-->


</body>

</html>
