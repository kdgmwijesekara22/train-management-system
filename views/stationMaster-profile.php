
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Station Master- Dashboard</title>
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">
    <!--    <link href="../dist/img/apple-touch-icon.png" rel="apple-touch-icon">-->

    <!-- Bootstrap core CSS-->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Page level plugin CSS-->
    <link href="../dist/lib/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../dist/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../dist/lib/animate/animate.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../dist/lib/css/core.min.css">
    <link href="../dist/lib/css/style.css" rel="stylesheet">
    <link href="../dist/lib/bootstrap/css/sb-admin.min.css" rel="stylesheet">
    <link href="../dist/lib/css/seatBookStyle.css" rel="stylesheet">
<style>
    .table {
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
    }

    .table thead th {
        font-weight: bold;
        text-align: center;
    }

    .table tbody td {
        text-align: center;
    }
    .table tbody td{
        color: black;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    }

    /* Dark mode styles */
    .dark-mode {
        background-color: #2e5a87;
        color: #2e5a87;
    }

    /* Sticky sidebar */
    .sidebar {
        position: sticky;
        top: 0;
        height: 100vh;
        overflow-y: auto;
    }

    /* Toggle switch styles */
    .switch {
        position: relative;
        display: inline-block;
        width: 40px;
        height: 20px;
        margin-right: 10px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #2e5a87;
        border-radius: 20px;
        transition: 0.4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 16px;
        width: 16px;
        left: 2px;
        bottom: 2px;
        background-color: #ffffff;
        border-radius: 50%;
        transition: 0.4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        transform: translateX(20px);
    }

</style>
</head>





<body id="page-top">

<?php require_once 'employeNavbar.php'; ?>

<div id="wrapper">

    <?php require_once 'employeeSideBar.php'; ?>

    <div id="content-wrapper">
        <div class="container-fluid" style="margin-top: 2.9rem">

            <div class="card mb-3" style="background-color: lightskyblue">
                <div class="card-header">
                    <i class="fas fa-users-cog" style="font-size:xxx-large;"></i>
                    <h3 style="color: #0b0b0b;">My Profile</h3>
                </div>
            </div>


            <div class="toggle-container">
                <label class="switch">
                    <input type="checkbox" id="darkModeToggle">
                    <span class="slider round"></span>
                </label>
                <span id="modeLabel">Dark Mode</span>
            </div>


            <section id="about" style="color: #122b40; font-size: medium; background-color: lightblue;">


                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div id="login-card" class="card m-3 p-0 shadow-lg">
                            <div class="card-header">
                                <label id="title-font" style="color: #0b0b0b; font-size: large;">
                                    <i class="fas fa-users-cog"></i>
                                    Station Master Profile
                                </label>
                            </div>


                            <div class="card-body" style="background-color:lightcyan;">
                                <form>
                                    <div class="form-group row">
                                        <label for="userNameProfile" class="col-sm-3 col-form-label">User Name:</label>
                                        <div class="col-sm-9">
                                            <label id="userNameProfile" for="" class="col-form-label"></label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="userEmailProfile" class="col-sm-3 col-form-label">E-mail:</label>
                                        <div class="col-sm-9">
                                            <label id="userEmailProfile" for="" class="col-form-label"></label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="userNicProfile" class="col-sm-3 col-form-label">NIC:</label>
                                        <div class="col-sm-9">
                                            <label id="userNicProfile" for="" class="col-form-label"></label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="userMobileProfile" class="col-sm-3 col-form-label">Mobile:</label>
                                        <div class="col-sm-9">
                                            <label id="userMobileProfile" for="" class="col-form-label"></label>
                                        </div>
                                    </div>

                                    <div class="form-row row my-3 justify-content-center">
                                        <div class="col-sm-6">
                                            <button class="btn btn-success btn-block" type="button" data-toggle="modal" data-target="#editDetails"
                                                    data-whatever="@getbootstrap">
                                                Edit Profile Details
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Left-side card for announcements -->
                        <div class="card m-3 p-3 shadow-lg" style="background-color: lightcyan;">
                            <div class="card-header">
                                <h5 class="card-title" style="color: black;">Announcements</h5>
                            </div>
                            <div class="card-body">

                                <p style="color: #1a1a1a;font-size: medium;"> A strike is planned on 2nd August 2023. </p>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!-- Right-side card for current train location -->
                        <div class="card m-3 p-3 shadow-lg" style="background-color: lightcyan;">
                            <div class="card-header">
                                <h5 class="card-title" style="color: black;" >Current Train Location</h5>
                            </div>
                            <div class="card-body">
                              current train location: <b style="font-size:large; color: #031c2d;font-family: 'Arial Narrow';"> Colombo Maradana Station<b/>
                            </div>
                        </div>
                    </div>
                </div>
        </div>





    </div


</div>

<!--edit details model-->
<div class="modal fade" id="editDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#2e5a87;">
                <h5 class="modal-title text-white" id="exampleModalLabel">My Profile Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <!-- from end-->

                <form class="my-3 p-4" style="background-color: #afafe7; border-radius: 8px;">
                    <div class="form-row">
                        <div class="col-6">
                            <label for="modalUsername" class="col-sm-3 col-form-label"><i class="fas fa-user"></i> </label>
                            <input id="modalUsername" type="text" class="form-control" placeholder="User Name"
                                   style="margin-bottom:10px;"
                                   data-toggle="tooltip" title="User Name"
                                   value="<?php echo $_SESSION['employee']?>"
                                   readonly>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="modalFirstName"><i class="fas fa-user"></i> First Name:</label>
                            <input id="modalFirstName" type="text" class="form-control" placeholder="Enter First Name" required pattern="[A-Za-z]+" title="Only alphabetic characters are allowed">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="modalLastName"><i class="fas fa-user"></i> Last Name:</label>
                            <input id="modalLastName" type="text" class="form-control" placeholder="Enter Last Name" required pattern="[A-Za-z]+" title="Only alphabetic characters are allowed">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="modalEmail"><i class="fas fa-envelope"></i> Email:</label>
                        <input id="modalEmail" type="email" class="form-control" placeholder="Enter Email">
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="modalPhoneNumber"><i class="fas fa-phone"></i> Mobile No:</label>
                            <input id="modalPhoneNumber" type="text" class="form-control" placeholder="Enter Mobile No">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="modalNic"><i class="far fa-id-card"></i> NIC:</label>
                            <input id="modalNic" type="text" class="form-control" placeholder="Enter NIC">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="password"><i class="fas fa-lock"></i> Password:</label>
                            <input id="password" type="password" class="form-control" placeholder="Enter New Password">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="modalconfirmpassword"><i class="fas fa-lock"></i> Confirm Password:</label>
                            <input id="modalconfirmpassword" type="password" class="form-control" placeholder="Confirm New Password">
                        </div>
                    </div>
                </form>

                <!-- from end-->

            </div>
            <div class="modal-footer" style="background-color:#2e5a87">
                <button type="button" class="btn btn-secondary float-left" data-dismiss="modal" style="background-color: #0c5460;">Close</button>
                <button id="updateModalEmployee" type="button" class="btn btn-dark" style="background-color: lightseagreen;">Update Detail</button>
            </div>
        </div>
    </div>
</div>

<?php
require_once 'footer.php';
?>
<!-- /.content-wrapper -->


<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>-->
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="logout">Logout</a>
            </div>
        </div>
    </div>
</div>

</div>


<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<script>
    let userName = "<?php echo $id ?>";
</script>

<!-- Bootstrap core JavaScript-->
<script src="../dist/lib/jquery/jquery.min.js"></script>
<script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"
        integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i"
        crossorigin="anonymous"></script>
<script src="../dist/controller/configuration.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../dist/controller/SMProfileController.js"></script>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });



        // Function to toggle dark mode
        function toggleDarkMode() {
        const body = document.body;
        body.classList.toggle('dark-mode');
        // Update the label text based on the current mode
        const modeLabel = document.getElementById('modeLabel');
        if (body.classList.contains('dark-mode')) {
        modeLabel.textContent = 'Light Mode';
    } else {
        modeLabel.textContent = 'Dark Mode';
    }
    }

        // Check if the user has a preferred color scheme (dark mode or light mode) set in their browser
        const userPrefersDark = window.matchMedia('(prefers-color-scheme: dark)').matches;

        // Toggle dark mode based on user preference (if they have dark mode enabled in their browser)
        if (userPrefersDark) {
        document.body.classList.add('dark-mode');
        document.getElementById('modeLabel').textContent = 'Light Mode';
    }

        // Add event listener to the dark mode toggle button
        const darkModeToggle = document.getElementById('darkModeToggle');
        darkModeToggle.addEventListener('click', toggleDarkMode);

</script>

</body>

</html>
