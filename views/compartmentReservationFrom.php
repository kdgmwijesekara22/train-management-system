<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Compartment Reservation Details

    </div>
</div>
<div class="container">
    <table class="table table-hover">
        <thead>
        <tr>
            <th class="text-center bg-secondary text-white">Train Name</th>
            <th class="text-center bg-secondary text-white">Compartment</th>
            <th class="text-center bg-secondary text-white">User Name</th>
            <th class="text-center bg-secondary text-white">From</th>
            <th class="text-center bg-secondary text-white">To</th>
            <th class="text-center bg-secondary text-white">Date</th>
            <th class="text-center bg-secondary text-white">Action</th>

        </tr>
        </thead>
        <tbody id="allTrainCompartmentRequestDetails">

        </tbody>

    </table>
</div>

<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>

</body>
</html>