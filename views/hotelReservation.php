<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../dist/lib/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="../dist/lib/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../dist/lib/bootstrap/css/sb-admin.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Requiring navBar-->
<?php require_once 'userNavBar.php'; ?>

<div id="wrapper">

    <!-- Sidebar -->

    <!-- Requiring side Bar panel-->
    <?php require_once 'userSideBar.php'; ?>

    <div id="content-wrapper">

        <div class="container-fluid" style="margin-top: 2.9rem">

            <!-- Breadcrumbs-->
            <div class="card mb-3" style="background-color: lightskyblue">
                <div class="card-header">
                    <i class="fas fa-users-cog"></i>
                    Resort Reservations
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <form class="col-md-7">

                            <div class="form-row">
                                <div class="col-md-6">
                                    <select class="custom-select mr-sm-2" id="resorts">
                                        <option selected>Select Resort</option>
                                        <option value="Nuwara Eliya">Nuwara Eliya</option>
                                        <option value="Katharagama">Katharagama</option>
                                        <option value="Badulla">Badulla</option>
                                    </select>

                                </div>

                                <div class="col-md-6">
                                    <input id="passenName" type="text" class="form-control" placeholder=" Your Name">
                                </div>


                            </div>

                            <br>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <label for="">From</label>
                                    <input id="startDate" type="date" class="form-control" placeholder="">
                                </div>
                                <div class="col-md-6">
                                    <label for="">To</label>
                                    <input id="endDate" type="date" class="form-control" placeholder="">
                                </div>
                            </div>

                            <br>
                            <div class="form-row">
                                <div class="col-md-5">
                                    <input id="mobiNo" type="text" class="form-control" placeholder="Mobile No">
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col-md-5">
                                    <input id="passNic" type="text" class="form-control" placeholder="NIC">
                                </div>
                            </div>
                            <br>


                            <br>
                            <div class="form-row form-control-lg">
                                <div class="col-md-2" id="reserveBtnArea">
                                    <button id="reservResortBtn" type="button" class="btn btn-success float-right "
                                            data-toggle="modal"
                                            data-target="#exampleModal"
                                            data-whatever="@getbootstrap"><i class="fas fa-user-edit"> </i>Reserve
                                    </button>
                                </div>


                            </div>
                            <br>


                        </form>
                        <div class="card col-md-5">
                            <div class="card-body">
                                <div id="message-scroller" class="card-body p-2"
                                     style="height:60vh;width:100%;overflow:auto;background-color:#d9d9d9;color:white;scrollbar-base-color:#d9d9d9;">
                                    <ul id="messagebody" class="list-group border-0 p-0"
                                        style="background-color:#d9d9d9;!important;">
                                        <li class="list-group-item border-0">

                                            <div class="card shadow-lg">
                                                <img src="../dist/img/hotel/cape.jpg " style="height: 200px"
                                                     class="card-img-top" alt="...">

                                                <div class="card-body text-center">
                                                    <h5 class="card-title text-dark"
                                                        style="font-family: 'Comic Sans MS'"><B>Cape
                                                            Resort</B></h5>
                                                    <p class="text-secondary ">Located on Beach Avenue, the
                                                        newly-renovated Beach Shack has the
                                                        feel of a fresh island retreat. Now open through December</p>

                                                </div>
                                                <div class="card-footer text-center">
                                                    <a id="userMainReserveNowCard">
                                                        <button class="btn btn-outline-info m-2" type="button">Reserve
                                                            Now
                                                        </button>
                                                    </a>

                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item border-0">
                                            <div class="card shadow-lg">
                                                <img src="../dist/img/hotel/Marriott3.jpg" style="height: 200px"
                                                     class="card-img-top" alt="...">

                                                <div class="card-body text-center">
                                                    <h5 class="card-title text-dark"
                                                        style="font-family: 'Comic Sans MS'">
                                                        Marriot</h5>
                                                    <p class="text-secondary">The awe-inspiring beauty of Sri Lanka is
                                                        on full display at
                                                        Weligama Bay Marriott Resort & Spa. Located on Weligama Bay,
                                                        moments from Mirissa Beach</p>
                                                </div>
                                                <div class="card-footer text-center">
                                                    <a id="userMainReserveNowCard">
                                                        <button class="btn btn-outline-info m-2" type="button">Reserve
                                                            Now
                                                        </button>
                                                    </a>

                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item border-0">

                                            <div class="card shadow-lg">
                                                <img src="../dist/img/hotel/fishermans-bay.jpg" style="height: 200px"
                                                     class="card-img-top" alt="...">
                                                <div class="card-body text-center">
                                                    <h5 class="card-title text-dark"
                                                        style="font-family: 'Comic Sans MS'">
                                                        Fisherman Bay Resort</h5>
                                                    <p class="text-secondary">At Bay Beach Hotel, you will find a
                                                        garden, barbeque facilities
                                                        and a cosy outdoor terrace. Guests can also utilize the meeting
                                                        facilities and luggage storage space. </p>

                                                </div>
                                                <div class="card-footer text-center">
                                                    <a id="userMainReserveNowCard">
                                                        <button class="btn btn-outline-info m-2" type="button">Reserve
                                                            Now
                                                        </button>
                                                    </a>

                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item border-0">

                                            <div class="card shadow-lg">
                                                <img src="../dist/img/hotel/88902727.jpg" style="height: 200px"
                                                     class="card-img-top" alt="...">
                                                <div class="card-body text-center">
                                                    <h5 class="card-title text-dark"
                                                        style="font-family: 'Comic Sans MS'">Palm Resort</h5>
                                                    <p class="text-secondary">Located in Weligama, just a short stroll
                                                        from the nearest beach,
                                                        Bay Beach Hotel features an in-house restaurant with a variety
                                                        of international cuisines .</p>
                                                </div>
                                                <div class="card-footer text-center">
                                                    <a id="userMainReserveNowCard">
                                                        <button class="btn btn-outline-info m-2" type="button">Reserve
                                                            Now
                                                        </button>
                                                    </a>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!--     resort reservation pop up-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-dark">
                        <h5 class="modal-title text-white" id="exampleModalLabel">Resort Reservations</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="text-center font-weight-bold">

                                <label for="refNo" class="">Your Reference No:</label>
                                <label id="refNo" for="" class="text-danger">45698626</label>
                            </div>
                            <div class="form-row m-3">

                                <label for="selectResort" class="">Selected Resort : </label>
                                <label id="selectResort" for="" class="text-primary"></label>
                            </div>
                            <div class="form-row m-3">

                                <label for="passengerName" class="">Passenger Name : </label>
                                <label id="passengerName" for="" class="text-primary"></label>
                            </div>
                            <div class="form-row m-3">

                                <label for="from" class="">From: </label>
                                <label id="from" for="" class="text-primary col-md-4"></label>
                                <label for="to" class="">To : </label>
                                <label id="to" for="" class="text-primary col-md-4"></label>
                            </div>
                            <div class="form-row m-3">

                                <label for="mobileNo" class="">Mobile No : </label>
                                <label id="mobileNo" for="" class="text-primary"></label>
                            </div>
                            <div class="form-row m-3">

                                <label for="nicNo" class="">NIC : </label>
                                <label id="nicNo" for="" class="text-primary"></label>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="float-left">
                            <button id="" type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                        </div>
                        <div>
                            <button id="submit-Reservation" type="button" class="btn btn-success">Submit</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Sticky Footer -->

        <?php
        require_once 'footer.php';
        ?>
    </div>
    <!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="">Logout</a>
            </div>
        </div>
    </div>
</div>

<script>
    let userName = "<?php echo $id ?>";
</script>

<!-- Bootstrap core JavaScript-->
<script src="../dist/lib/jquery/jquery.min.js"></script>
<script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="../dist/lib/jquery-easing/jquery.easing.min.js"></script>
<script src="../dist/controller/configuration.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../dist/controller/userProfileController.js"></script>
<script src="../dist/controller/resortReservationController.js"></script>

<!-- Custom scripts for all pages-->
<script src="../dist/lib/bootstrap/js/sb-admin.min.js"></script>

</body>

</html>
