<ul class="sidebar navbar-nav fixed-nav" style="margin-top: 3.5rem;">
    <li class="nav-item active">
        <a id="userSideBarDashboard" class="nav-link">
            <i class="fas fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item">
        <a id="userSidebarUserProf" class="nav-link">
            <i class="fas fa-user-circle"></i>
            <span>My Profile</span>
        </a>
    </li>
    <li class="nav-item">
        <a id="userSideBarTicketReservation" class="nav-link">
            <i class="fas fa-ticket-alt"></i>
            <span>Seat Booking</span>
        </a>
    </li>


    <li class="nav-item ">
        <a id="userSideBarCompartmentRev" class="nav-link">
            <i class="fas fa-train"></i>
            <span>Compartment Reservation</span>
        </a>
    </li>
    <li class="nav-item ">
        <a id="userSideBarReservation" class="nav-link">
            <i class="fas fa-train"></i>
            <span>Train Reservation</span>
        </a>
    </li>
    <li class="nav-item ">
        <a id="userSideBarHotelReservation" class="nav-link">
            <i class="fas fa-hotel"></i>
            <span>Resort Reservations</span>
        </a>
    </li>
    <li class="nav-item ">
        <a id="userSideBarRetireRoomReservation" class="nav-link">
            <i class="fas fa-home"></i>
            <span>Retire-Room Reservations</span>
        </a>
    </li>

    <li class="nav-item">
        <a id="userSideBarHistory" class="nav-link">
            <i class="fas fa-eye"></i>
            <span>History</span>
        </a>
    </li>

    <li class="nav-item">
        <a id="userSideBarLogout" class="nav-link">
            <i class="fas fa-sign-out-alt"></i>
            <span>Log Out</span>
        </a>
    </li>

</ul>