<!DOCTYPE html>
<html lang="en">

<head>

    <style>
        .table {
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
        }

        .table thead th {
            font-weight: bold;
            text-align: center;
        }

        .table tbody td {
            text-align: center;
        }
        .table tbody td{
            color: black;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        /* Dark mode styles */
        .dark-mode {
            background-color: #2e5a87;
            color: #fff;
        }

        /* Sticky sidebar */
        .sidebar {
            position: sticky;
            top: 0;
            height: 100vh;
            overflow-y: auto;
        }

        /* Toggle switch styles */
        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 20px;
            margin-right: 10px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #2e5a87;
            border-radius: 20px;
            transition: 0.4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 16px;
            width: 16px;
            left: 2px;
            bottom: 2px;
            background-color: #ffffff;
            border-radius: 50%;
            transition: 0.4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            transform: translateX(20px);
        }



    </style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Dashboard</title>
    <!-- Favicons -->
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">


    <!-- Bootstrap core CSS-->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Page level plugin CSS-->
    <link href="../dist/lib/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../dist/lib/bootstrap/css/sb-admin.min.css" rel="stylesheet">
    <link href="../dist/lib/css/style.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Requiring navBar-->
<?php require_once 'navBar.php'; ?>

<div id="wrapper">

    <!-- Sidebar -->

    <!-- Requiring side Bar panel-->
    <?php require_once 'sideBar.php'; ?>

    <div id="content-wrapper">

        <div class="container-fluid" style="margin-top: 2.9rem">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a id="admin-Dash">Dashboard</a>
                </li>
                <li class="breadcrumb-item active" style="color: #0c5460;">Manage Station Master</li>
            </ol>
            <h1 class="text-secondary" >Manage Station Master</h1>
            <hr>


            <div class="toggle-container">
                <label class="switch">
                    <input type="checkbox" id="darkModeToggle">
                    <span class="slider round"></span>
                </label>
                <span id="modeLabel">Dark Mode</span>
            </div>

            <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    <b style="color: #0c5460;">Station Master Details Summary</b>
                    <button type="button" class="btn btn-success float-right" data-toggle="modal" style="background-color: #0e63b0;"
                            data-target="#exampleModal"
                            data-whatever="@getbootstrap"><i class="fas fa-user-edit"> </i>Add New
                    </button>
                </div>
            </div>
            <div class="container">
                <table class="table table-hover">

                    <thead>
                    <tr>

                        <th class="text-center bg-secondary text-white">First Name</th>
                        <th class="text-center bg-secondary text-white">Last Name</th>
                        <th class="text-center bg-secondary text-white">User Name</th>
                        <th class="text-center bg-secondary text-white">Email</th>
                        <th class="text-center bg-secondary text-white">NIC</th>
                        <th class="text-center bg-secondary text-white">Mobile No</th>
                        <th class="text-center bg-secondary text-white">Action</th>
                    </tr>
                    </thead>
                    <tbody id="userTable">

                    </tbody>

                </table>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <!-- Sticky Footer -->
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#808080">
                <h5 class="modal-title text-white" id="exampleModalLabel">Manage Station Master</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>



            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="passegerFirstName" class="col-sm-3 col-form-label">First Name</label>
                        <div class="col-sm-9">
                            <input id="passegerFirstName" type="text" class="form-control" placeholder="Enter first name" required
                                   pattern="[A-Za-z]+" title="Only alphabetic characters are allowed">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="passegerSecondName" class="col-sm-3 col-form-label">Last Name</label>
                        <div class="col-sm-9">
                            <input id="passegerSecondName" type="text" class="form-control" placeholder="Enter last name" required
                                   pattern="[A-Za-z]+" title="Only alphabetic characters are allowed">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="passengerName" class="col-sm-3 col-form-label">User Name</label>
                        <div class="col-sm-9">
                            <input id="passengerName" type="text" class="form-control" placeholder="Enter user name" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="passengerEmail" class="col-sm-3 col-form-label">E-mail</label>
                        <div class="col-sm-9">
                            <input id="passengerEmail" type="email" class="form-control" placeholder="Enter e-mail address" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="passengerPhone" class="col-sm-3 col-form-label">Mobile No</label>
                        <div class="col-sm-9">
                            <input id="passengerPhone" type="text" class="form-control" placeholder="Enter mobile number"
                                   required maxlength="10" pattern="[0-9]{10}" title="Mobile number should be 10 digits and contain only numbers">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="passegerNIC" class="col-sm-3 col-form-label">NIC</label>
                        <div class="col-sm-9">
                            <input id="passegerNIC" type="text" class="form-control" placeholder="Enter NIC" required maxlength="12">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="passegerPassword" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input id="passegerPassword" type="password" class="form-control" placeholder="Enter password" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="confirm-password" class="col-sm-3 col-form-label">Confirm Password</label>
                        <div class="col-sm-9">
                            <input id="confirm-password" type="password" class="form-control" placeholder="Confirm password" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary">Check</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </div>
                </form>


            </div>
            <div class="modal-footer">
                <div class="float-left">
                    <button id="clearBTN" type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                </div>
                <div>
                    <button id="createPassengerAccount" type="button" class="btn btn-success">Submit</button>
                </div>

            </div>
        </div>
    </div>
</div>


<!--edit details model-->
<div class="modal fade" id="editDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #2e5a87; color: #fff;">
                <h5 class="modal-title" id="exampleModalLabel">Edit Station Master Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>

                    <div class="form-group row">
                        <label for="editfirst-Name" class="col-sm-3 col-form-label">First Name</label>
                        <div class="col-sm-9">
                            <input id="editfirst-Name" type="text" class="form-control" placeholder="Enter first name" >
                        </div>



                    </div>
                    <div class="form-group row">
                        <label for="editlast-Name" class="col-sm-3 col-form-label">Last Name</label>
                        <div class="col-sm-9">
                            <input id="editlast-Name" type="text" class="form-control" placeholder="Enter last name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="edituser-Name" class="col-sm-3 col-form-label">User Name</label>
                        <div class="col-sm-9">
                            <input id="edituser-Name" type="text" class="form-control" placeholder="Enter user name" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="editemail" class="col-sm-3 col-form-label">E-mail</label>
                        <div class="col-sm-9">
                            <input id="editemail" type="email" class="form-control" placeholder="Enter e-mail address" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="editmobile" class="col-sm-3 col-form-label">Mobile No</label>
                        <div class="col-sm-9">
                            <input id="editmobile" type="text" class="form-control" placeholder="Enter mobile number">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="editnic" class="col-sm-3 col-form-label">NIC</label>
                        <div class="col-sm-9">
                            <input id="editnic" type="text" class="form-control" placeholder="Enter NIC">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="editpassword" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input id="editpassword" type="password" class="form-control" placeholder="Enter password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="editconfirm-password" class="col-sm-3 col-form-label">Confirm Password</label>
                        <div class="col-sm-9">
                            <input id="editconfirm-password" type="password" class="form-control" placeholder="Confirm password">
                        </div>
                    </div>



                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary float-left" data-dismiss="modal">Close</button>
                <button type="button" id="updateEmp" class="btn btn-primary">Update Details</button>
            </div>
        </div>
    </div>
</div>


<?php require_once 'footer.php'; ?>

<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>-->
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" id="logout">Logout</a>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript-->
<script src="../dist/lib/jquery/jquery.min.js"></script>
<script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>

<script>




    // Function to toggle dark mode
    function toggleDarkMode() {
        const body = document.body;
        body.classList.toggle('dark-mode');
        // Update the label text based on the current mode
        const modeLabel = document.getElementById('modeLabel');
        if (body.classList.contains('dark-mode')) {
            modeLabel.textContent = 'Light Mode';
        } else {
            modeLabel.textContent = 'Dark Mode';
        }
    }

    // Check if the user has a preferred color scheme (dark mode or light mode) set in their browser
    const userPrefersDark = window.matchMedia('(prefers-color-scheme: dark)').matches;

    // Toggle dark mode based on user preference (if they have dark mode enabled in their browser)
    if (userPrefersDark) {
        document.body.classList.add('dark-mode');
        document.getElementById('modeLabel').textContent = 'Light Mode';
    }

    // Add event listener to the dark mode toggle button
    const darkModeToggle = document.getElementById('darkModeToggle');
    darkModeToggle.addEventListener('click', toggleDarkMode);


</script>


<!-- Demo scripts for this page-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../dist/controller/configuration.js"></script>
<script src="../dist/controller/manageEmployeeController.js"></script>
<!--<script src="../dist/controller/passengerController.js"></script>-->


</body>

</html>
