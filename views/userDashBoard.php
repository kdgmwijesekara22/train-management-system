<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User - Dashboard</title>

    <!-- Favicons -->
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">
    <!--    <link href="../dist/img/apple-touch-icon.png" rel="apple-touch-icon">-->

    <!-- Bootstrap core CSS-->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Page level plugin CSS-->
    <link href="../dist/lib/datatables/dataTables.bootstrap4.css" rel="stylesheet">


    <link href="../dist/lib/animate/animate.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../dist/lib/css/core.min.css">
    <link href="../dist/lib/css/style.css" rel="stylesheet">
    <link href="../dist/lib/bootstrap/css/sb-admin.min.css" rel="stylesheet">
    <link href="../dist/lib/css/seatBookStyle.css" rel="stylesheet">
    <style>
        /* Custom CSS to center the h4 in the card body */
        .centered-card-text {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
        }
    </style>

</head>

<body id="page-top">

<!-- Requiring navBar-->
<?php require_once 'userNavBar.php'; ?>

<div id="wrapper">

    <!-- Sidebar -->

    <!-- Requiring side Bar panel-->
    <?php require_once 'userSideBar.php'; ?>

    <div id="content-wrapper">

        <div class="container-fluid" style="margin-top: 2.9rem">

            <div class="card mb-3" style="background-color: lightskyblue">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Reservation Requests
                </div>
            </div>
            <!--            <div class="alert alert-success" role="alert">-->
            <!--                <h4 class="alert-heading">Well done!</h4>-->
            <!--                <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit-->
            <!--                    longer so that you can see how spacing within an alert works with this kind of content.</p>-->
            <!--                <hr>-->
            <!--                <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>-->
            <!--            </div>-->

            <div class="container-fluid" style="margin-top: 2.0rem">

                <!-- Added colorful cards -->
                <div class="row mb-3">
                    <div class="col-lg-3 col-md-6">
                        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                            <div class="card-header"><h5 class="card-title">Compartment Reservation Requests</h5></div>
                            <div class="card-body">
                                <!--                                <i class="fas fa-train"></i>-->
                                    <h4 class="card-text centered-card-text">05</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                            <div class="card-header"><h5 class="card-title">Train Reservation Requests</h5></div>
                            <div class="card-body">
                                <!--                                <i class="fas fa-train"></i>-->
                                <h4 class="card-text centered-card-text">05</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                            <div class="card-header"><h5 class="card-title">Seat Reservations Requests</h5></div>
                            <div class="card-body">
                                <!--                                <i class="fas fa-train"></i>-->
                                <h4 class="card-text centered-card-text">05</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                            <div class="card-header"><h5 class="card-title">All Reservations Requests</h5></div>
                            <div class="card-body">
                                <!--                                <i class="fas fa-train"></i>-->
                                <h4 class="card-text centered-card-text">05</h4>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="card m-2">
                    <div class="card-header">
                        <label class="card-title text-info">
                            <i class="fas fa-train"></i>
                            Compartment Reservation Requests
                        </label>
                        <button class="btn bg-transparent float-right text-black-50" data-toggle="collapse"
                                href="#collaps-customerReport" role="button" aria-expanded="false"
                                aria-controls="collaps-customerReport">
                            <i class="far fa-minus-square"></i>
                        </button>
                    </div>
                    <div class="collapse" id="collaps-customerReport">
                        <div class="card-body" id="compartmentRequests">

                            <p class="text-center text-danger">No current Compartment Requests.</p>


                        </div>
                    </div>
                </div>

                <!--                        For Train reservation-->
                <div class="card m-2">
                    <div class="card-header">
                        <label class="card-title text-info">
                            <i class="fas fa-train"></i>
                            Train Reservation Requests
                        </label>
                        <button class="btn bg-transparent float-right text-black-50" data-toggle="collapse"
                                href="#collaps-train" role="button" aria-expanded="false"
                                aria-controls="collaps-customerReport">
                            <i class="far fa-minus-square"></i>
                        </button>
                    </div>
                    <div class="collapse" id="collaps-train">
                        <div class="card-body" id="trainRequests">

                            <p class="text-center text-danger">No current Train Requests.</p>

                        </div>
                    </div>
                </div>


                <!--            klklklk-->

                <div class="card m-2">
                    <div class="card-header">
                        <label class="card-title text-info">
                            <i class="fas fa-train"></i>
                            Seat Reservations
                        </label>
                        <button class="btn bg-transparent float-right text-black-50" data-toggle="collapse"
                                href="#collaps" role="button" aria-expanded="false"
                                aria-controls="collaps-customerReport">
                            <i class="far fa-minus-square"></i>
                        </button>
                    </div>
                    <div class="collapse" id="collaps">
                        <div class="card-body" id="seatbookings">

                            <p class="text-center text-danger">No current Seat Bookings.</p>

                        </div>
                    </div>
                </div>
            </div>


        </div>

        <?php
        require_once 'footer.php';
        ?>
        <!-- /.content-wrapper -->

    </div>
    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <!--            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>-->
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" id="logout">Logout</a>
                </div>
            </div>
        </div>
    </div>


    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <script>
        let userName = "<?php echo $id ?>";
    </script>


    <!-- Bootstrap core JavaScript-->
    <script src="../dist/lib/jquery/jquery.min.js"></script>
    <script src="../dist/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src =
            "https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js">
    </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="../dist/controller/configuration.js"></script>
    <script src="../dist/controller/userProfileController.js"></script>
    <script src="../dist/controller/userDashboardController.js"></script>
    <script src="../dist/controller/passengerController.js"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>

</body>

</html>
