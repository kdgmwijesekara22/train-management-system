<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <title>Admin Manage Train Request</title>
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">

    <!--fav icon-->
    <link href="../dist/img/train-electric-fast-512.png" rel="icon">

    <!-- Bootstrap core CSS-->
    <link href="../dist/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../dist/lib/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="../dist/lib/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../dist/lib/bootstrap/css/sb-admin.css" rel="stylesheet">
    <link href="../dist/lib/css/style.css" rel="stylesheet">

</head>
<body>
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Train Reservation Details

    </div>
</div>
<div class="container">
    <table class="table table-hover">
        <thead>
        <tr>
            <th class="text-center bg-secondary text-white">Reserve ID</th>
            <th class="text-center bg-secondary text-white">User Name</th>
            <th class="text-center bg-secondary text-white">Train Name</th>
            <th class="text-center bg-secondary text-white">From</th>
            <th class="text-center bg-secondary text-white">TO</th>
            <th class="text-center bg-secondary text-white">Reserve Date</th>
            <th class="text-center bg-secondary text-white">Action</th>
        </tr>
        </thead>
        <tbody id="trainReservationTable">


    </table>
</div>

    <div class="modal fade" id="acceptModals" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-secondary">
                <h5 class="modal-title text-white" id="exampleModalLabel">Accepting Request..!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-footer bg-secondary">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="accept" type="button" class="btn btn-primary">Send message</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="rejectModals" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title text-white" id="exampleModalLabel">Rejecting Request..!</h5>
                <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-footer bg-danger">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="reject" type="button" class="btn btn-primary">Reject Requesting</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>