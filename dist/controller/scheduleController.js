
//------------------------getAllSchedule-------------------------------------


function loadAllSchedule() {


    $('#scheduleTable').empty();
    $.ajax({
        type: "GET",
        url:  baseURL +'getAllSchedule',
        async: true,
        dataType: "json"
    }).done(function (responce) {

        for (let i in responce) {
            let schedule = responce[i];

            let trainName = schedule[0];
            let from = schedule[1];
            let to = schedule[2];
            let date = schedule[3];
            let time = schedule[4];
            let startTime = schedule[5];


            let tableRow = "<tr>" +
                "<td class='text-center text-primary'>" + trainName + "</td>" +
                "<td class='text-center'>"+ from + "</td>" +
                "<td class='text-center '>" + to + "</td>" +
                "<td class='text-center text-danger'>"+ date + "</td>" +
                "<td class='text-center text-success'>" + time + "</td>" +
                "<td class='text-center text-success'>" + startTime + "</td>"+
                "</tr>";

            $('#scheduleTable').append(tableRow);
        }
    });
}

loadAllSchedule();