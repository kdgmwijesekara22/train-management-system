function searchCity() {
    $.ajax({
        type: "POST",
        url: baseURL + 'searchTrainUserDashboard',
        async: true,
        dataType: "json",
        data: {
            city: $('#startCity').val()
        },
    }).done(function (responce) {
        for (var i in responce) {
            var city = responce[i];

            $('#mainStation1').text(city[0]);
        }

    });
}

$(document).ready(function () {


    $('#startCity').keypress(function () {
        searchCity();
    });

    $('#destination').keypress(function () {
        $.ajax({
            type: "POST",
            url: baseURL + 'searchTrainUserDashboard',
            async: true,
            dataType: "json",
            data: {
                city: $('#destination').val()


            },
        }).done(function (responce) {
            for (var i in responce) {
                var city = responce[i];

                $('#mainStation2').text(city[0]);
            }

        });
    });


});


//-----------------reservation transaction--------------------------------

$(document).ready(function () {

    $('#createPassengerAccount').click(function () {

        $.ajax({
            type: "POST",
            url: baseURL + 'addPassenger',
            data: {
                username: $("#passengerName").val(),
                firstName: $("#passegerFirstName").val(),
                lastName: $("#passegerSecondName").val(),
                password: $("#passegerPassword").val(),
                email: $("#passengerEmail").val(),
                nic: $("#passegerNIC").val(),
                phoneNumber: $("#passengerPhone").val()

            },
            success: function (data) {

                clear();
            }
        });

    });

});

//-------------------userDashBoard Search-----------------------
var searchCompartment = new Array();
var bookingData = new Array();

function setDefaultDetail() {
    $('#holder').hide(500);
}

setDefaultDetail();

$('#userSearch').click(function () {

    var meainSt1 = $("#mainStation1").text();
    var mainstaino2 = $("#mainStation2").text();
    var reserveDate = $("#reserveDate").val();

    $('#search-table').empty();
    $.ajax({
        type: "POST",
        url: baseURL + 'isAvailableTrainRequest',
        async: true,
        dataType: "json",
        data: {
            from: $("#mainStation1").text(),
            to: $("#mainStation2").text(),
            date: $('#reserveDate').val(),
        },
    }).done(function (responce) {
        for (let i in responce) {
            let details = responce[i];

            let trainName = details[0];
            let from = details[1];
            let to = details[2];
            let departureTime = details[4];
            let arrivalTime = details[5];

            let button = "<button  id=\"reserveBtn" + i + "\" type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\"\n" +
                "data-target=\"#exampleModalCenter\">\n" +
                "View Seats\n" +
                "</button>";


            let tableRow = "<tr>\n" +
                "                                    <td class=\"text-center\">" + trainName + "</td>\n" +
                "                                    <td class=\"text-center\">" + from + "</td>\n" +
                "                                    <td class=\"text-center\">" + to + "</td>\n" +
                "                                    <td class=\"text-center\">" + departureTime + "</td>\n" +
                "                                    <td class=\"text-center\">" + arrivalTime + "</td>\n" +
                "                                    <td class=\"text-center\">" + button + "</td>\n" +
                "                                </tr>";

            $('#search-table').append(tableRow);

            searchCompartment.push([details[0], details[1], details[2], details[4], details[3], details[5], details[6], details[7], details[8]]);
        }
        $.each(searchCompartment, function (index, value) {
            $('#reserveBtn' + index).click(function () {
                bookingData = searchCompartment[index];
            });
        });
        // var numberOfseats = 700;
        // var amount = "RS :" + 45000.00;
        // $('#numberOfSeat').val(numberOfseats);
        // $('#amount').val(amount);

    });
});

function sendMailWhenTrainReserve() {
    $.ajax({
        type: "GET",
        url: baseURL + 'getEmailAddressFromPassenger',
        async: true,
        dataType: "json",

    }).done(function (responce) {

        let email = responce;
        let reservationType = 'Train Reservation';
        let date = bookingData[4];

        sendingInfoMailToPassenger(email, reservationType, date);
    });
}

function sendingInfoMailToPassenger(email, reservationType, date) {

    $.ajax({
        type: "POST",
        url: baseURL + 'MailingSystem',
        async: true,
        dataType: "json",
        data: {
            email: email,
            reservationName: reservationType,
            date: date,
        },
    }).done(function (responce) {


    });

}
//--------------------------------make train reservation---------------------


var searchTrain = new Array();
var bookingData = new Array();

$(document).ready(function () {

    $('#userSearchTrain').click(function () {

        $('#userShedule').empty();

        $.ajax({
            type: "POST",
            url: baseURL + 'isAvailableTrainRequest',
            async: true,
            dataType: "json",
            data: {
                from: $("#mainStation1").text(),
                to: $("#mainStation2").text(),
                date: $("#reservationDate").val()
            },
        }).done(function (responce) {

            for (let i in responce) {
                let details = responce[i];

                let trainName = details[0];
                let from = details[1];
                let to = details[2];
                let startCity = details[3];
                let departureTime = details[4];
                let arrivalTime = details[5];
                let stationId = details[6];
                let className = details[7];


                let button = "<button  id=\"reserveBtn" + i + "\" type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModalCenter\">\n" +
                    "Reserve Now\n" +
                    "</button>";


                let tableRow = " <tr>\n" +
                    "                            <td class=\"text-center\">" + trainName + "</td>\n" +
                    "                            <td class=\"text-center\">" + from + "</td>\n" +
                    "                            <td class=\"text-center\">" + to + "</td>\n" +
                    "                            <td class=\"text-center\">" + departureTime + "</td>\n" +
                    "                            <td class=\"text-center\">" + arrivalTime + "</td>\n" +
                    "                            <td class=\"text-center\">" + button + "</td>\n" +
                    "                        </tr>";

                $('#userShedule').append(tableRow);

                searchTrain.push([details[0], details[1], details[2], details[4], details[3], details[5], details[6], details[7], details[8]]);
            }
            $.each(searchTrain, function (index, value) {
                $('#reserveBtn' + index).click(function () {
                    bookingData = searchTrain[index];


                });
            });
            var numberOfseats = 700;
            var amount = "RS :" + 45000.00;
            $('#numberOfSeat').val(numberOfseats);
            $('#amount').val(amount);

        });
    });
});

//--------------add a request----------------------------------------
$('#user-send-request').click(function () {
    $.ajax({
        type: "POST",
        url: baseURL + 'sendingTrainRequest',
        async: true,
        dataType: "json",
        data: {
            trainName: bookingData[0],
            from: bookingData[1],
            to: bookingData[2],
            reserveDateDate: bookingData[4],
            shedulID: bookingData[6],
            trainID: bookingData[7],
            status: "Train Reservation",
            requesting: "request",
        },
    }).done(function (responce) {

        if (responce == 1) {
            swal({
                title: "Success...!",
                text: "Your request has send successfully..!",
                icon: "success",

            });
            $('#exampleModalCenter').hide(200);
            $('#exampleModalCenter').modal('toggle');
            sendMailWhenTrainReserve();
        }
        else {
            swal({
                title: " Unsuccessful...!",
                text: "Something wrong check again your entered Details..!",
                icon: "error",

            });

        }

    });
});