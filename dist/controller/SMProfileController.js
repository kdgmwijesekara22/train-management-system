let global_Point=0;

function employeeProfile() {
    $.ajax({
        type: "GET",
        url:  baseURL +'getAllEmployeeDetailByEmpID',
        async: true,
        dataType: "json"
    }).done(function (responce) {

        for (var i in responce) {

            var temp = responce[i];

            let fName = temp[0];
            let lName = temp[1];
            let userDB = temp[2];
            let email = temp[5];
            let nic = temp[6];
            let mobile = temp[7];
            let point = temp[8];



            $('#userNameProfile').text(userName);
            $('#name').text(userName);
            $('#modalUser').val(userName);
            $('#modalFirstName').val(fName);
            $('#modalLastName').val(lName);
            $('#modalEmail').val(email);
            $('#modalPhoneNumber').val(mobile);
            $('#modalNic').val(nic);

            $('#userEmailProfile').text(email);
            $('#userNicProfile').text(nic);
            $('#userMobileProfile').text(mobile);
            $('#userPointsProfile').text(point);
            $('#point').text(point);

            global_Point = point;


        }
    });
}


employeeProfile();


$('#updateModalEmployee').click(function () {

    let edituserName = $('#modalUsername').val();
    let editFirstName = $('#modalFirstName').val();
    let editLastName = $('#modalLastName').val();
    let editPassword = $('#password').val();
    let editEmail = $('#modalEmail').val();
    let editNic = $('#modalNic').val();
    let editMobile = $('#modalPhoneNumber').val();
    let confirmPassword = $('#modalconfirmpassword').val();

    if (editPassword === confirmPassword) {
        $.ajax({
            type: "POST",
            url: baseURL + 'updateEmployeeDetail',
            async: true,
            data: {
                username: edituserName,
                firstName: editFirstName,
                lastName: editLastName,
                password: editPassword,
                email: editEmail,
                nic: editNic,
                mobile: editMobile,

            },
        }).done(function (resp) {

            if (resp == 1) {
                swal({
                    title: "Success...!",
                    text: "Your Account Has Been Updated..!",
                    icon: "success",

                });
                employeeProfile();
                clear2();
                $('#editDetails').hide(200);
                $('#editDetails').modal('toggle');
            } else {
                swal({
                    title: " Can't Update...!",
                    text: "Something Wrong Check Again..!",
                    icon: "error",
                });

            }
        });

    } else {
        swal({
            title: "Can't Update...!",
            text: "Password is not matching..!",
            icon: "error",
        });
    }


});

function clear2() {
    $('#modalUsername').val("");
    $('#modalLastName').val("");
    $('#password').val("");
    $('#modalEmail').val("");
    $('#modalNic').val("");
    $('#modalPhoneNumber').val("");
    $('#modalconfirmpassword').val("");
    $('#modalFirstName').val("");
}
