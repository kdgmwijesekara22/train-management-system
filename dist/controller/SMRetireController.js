

(function() {
    function IDGenerator() {

        this.length = 8;
        this.timestamp = +new Date;

        var _getRandomInt = function( min, max ) {
            return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
        }

        this.generate = function() {
            var ts = this.timestamp.toString();
            var parts = ts.split( "" ).reverse();
            var id = "";

            for( var i = 0; i < this.length; ++i ) {
                var index = _getRandomInt( 0, parts.length - 1 );
                id += parts[index];
            }

            return id;
        }


    }


    document.addEventListener( "DOMContentLoaded", function() {
        var btn = document.querySelector( "#reservResortBtn" ),
            output = document.querySelector( "#refNo" );

        btn.addEventListener( "click", function() {
            var generator = new IDGenerator();
            output.innerHTML = generator.generate();

        }, false);

    });


})();


$('#reservResortBtn').click(function () {
    let selectedresorts = $('#resorts').val();
    let passengerName = $('#passenName').val();
    let startDate = $('#startDate').val();
    let endDate = $('#endDate').val();
    let mobileNo = $('#mobiNo').val();
    let passengeNIC = $('#passNic').val();
    if (selectedresorts === 'Select Retire Room') {
        $('#resorts').css("border-color", "red");


    } else {
        $('#resorts').css("border-color", "blue");
    }
    if (passengerName == '') {
        $('#passenName').css("border-color", "red");


    } else {
        $('#passenName').css("border-color", "blue");

    }
    if (startDate == '') {
        $('#startDate').css("border-color", "red");

    } else {
        $('#startDate').css("border-color", "blue");
    }
    if (endDate == '') {
        $('#endDate').css("border-color", "red");

    } else {
        $('#endDate').css("border-color", "blue");
    }
    if (mobileNo == '') {
        $('#mobiNo').css("border-color", "red");

    } else {
        $('#mobiNo').css("border-color", "blue");
    }

    if (passengeNIC == '') {
        $('#passNic').css("border-color", "red");

    } else {
        $('#passNic').css("border-color", "blue");
    }

    if (!(selectedresorts == "") && !(passengerName == "") && !(startDate == "") &&
        !(endDate == "") && !(mobileNo == "") && !(passengeNIC == "")) {

        $('#selectResort').text(selectedresorts);
        $('#passengerName').text(passengerName);
        $('#from').text(startDate);
        $('#to').text(endDate);
        $('#mobileNo').text(mobileNo);
        $('#nicNo').text(passengeNIC);

    }


});


$('#submit-Reservation').click(function () {
    let selectedresorts = $('#resorts').val();
    let passengerName = $('#passenName').val();
    let startDate = $('#startDate').val();
    let endDate = $('#endDate').val();
    let mobileNo = $('#mobiNo').val();
    let passengeNIC = $('#passNic').val();

    if (!(selectedresorts == "") && !(passengerName == "") && !(startDate == "") &&
        !(endDate == "") && !(mobileNo == "") && !(passengeNIC == "")) {

        $('#selectResort').text('');
        $('#passengerName').text('');
        $('#from').text('');
        $('#to').text('');
        $('#mobileNo').text('');
        $('#nicNo').text('');
        $('#resorts').val('');
        $('#passenName').val('');
        $('#startDate').val('');
        $('#endDate').val('');
        $('#mobiNo').val('');
        $('#passNic').val('');
        let refno=$('#refNo').text();


        swal({
            title: "Success...!",
            text: "Your retire room reservation have been successfully submitted..!Your Reference No is "+refno+"",
            icon: "success",
        });
        $('#exampleModal').hide(200);
        $('#exampleModal').modal('toggle');


    }else{
        swal({
            title: "Unsuccessful...!",
            text: "Something wrong check again..!",
            icon: "error",
        });
    }


});

