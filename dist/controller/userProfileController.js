let global_Point=0;

function userProfile() {
    $.ajax({
        type: "GET",
        url:  baseURL +'getUserProfile',
        async: true,
        dataType: "json"
    }).done(function (responce) {
        for (var i in responce) {

            var temp = responce[i];

            let fName = temp[0];
            let lName = temp[1];
            let userDB = temp[2];
            let email = temp[5];
            let nic = temp[6];
            let mobile = temp[7];
            let point = temp[8];


            if (userDB=== userName && temp[3] === 'Passenger') {

                $('#userNameProfile').text(userName);
                $('#name').text(userName);
                $('#modalUser').val(userName);
                $('#modalFirstName').val(fName);
                $('#modalLastName').val(lName);
                $('#modalEmail').val(email);
                $('#modalPhoneNumber').val(mobile);
                $('#modalNic').val(nic);

                $('#userEmailProfile').text(email);
                $('#userNicProfile').text(nic);
                $('#userMobileProfile').text(mobile);
                $('#userPointsProfile').text(point);
                $('#point').text(point);

                global_Point = point;

            }
        }
    });
}


userProfile();