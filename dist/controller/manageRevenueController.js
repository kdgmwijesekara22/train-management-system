//----------------clear Passenger----------------


function clearEmployee() {
    $("#passengerName").val("");
    $("#passengerEmail").val("");
    $("#passegerFirstName").val("");
    $("#passegerSecondName").val("");
    $("#passegerNIC").val("");
    $("#passengerPhone").val("");
    $("#passegerPassword").val("");
    $("#confirm-password").val("");
}
$(".btn-warning").click(function() {
    $("#createPassengerAccount").text("Edit");
});

//----------------------add  Employeee---------------------


$('#createPassengerAccount').click(function () {

    let passengerName = $("#passengerName").val();
    let firstName = $("#passegerFirstName").val();
    let passengerSecondName = $("#passegerSecondName").val();
    let password = $("#passegerPassword").val();
    let email = $("#passengerEmail").val();
    let passengeNIC = $("#passegerNIC").val();
    let mobileno = $("#passengerPhone").val();
    let confirmpass = $('#confirm-password').val();


    if (passengerName === '') {
        $('#passengerName').css("border-color", "red");

    } else {
        $('#passengerName').css("border-color", "blue");
    }


    if (firstName === '') {
        $('#passegerFirstName').css("border-color", "red");

    } else {
        $('#passegerFirstName').css("border-color", "blue");
    }


    if (passengerSecondName === '') {
        $('#passegerSecondName').css("border-color", "red");

    } else {
        $('#passegerSecondName').css("border-color", "blue");
    }

    if (password === '') {
        $('#passegerPassword').css("border-color", "red");

    } else {
        $('#passegerPassword').css("border-color", "blue");
    }

    if (mobileno === '') {
        $('#passengerPhone').css("border-color", "red");

    } else {
        $('#passengerPhone').css("border-color", "blue");
    }

    if (passengeNIC === '') {
        $('#passegerNIC').css("border-color", "red");

    } else {
        $('#passegerNIC').css("border-color", "blue");
    }
    if (email === '') {
        $('#passengerEmail').css("border-color", "red");

    } else {
        $('#passengerEmail').css("border-color", "blue");
    }

    if (!(passengerName == "") && !(firstName == "") && !(passengerSecondName == "") &&
        !(password == "") && !(mobileno == "") && !(passengeNIC == "") && !(email == "")) {

        addNewEmployees();

    }


});

function addNewEmployees() {
    $.ajax({
        type: "POST",
        url: baseURL + 'addNewEmployee',
        data: {
            username: $("#passengerName").val(),
            firstName: $("#passegerFirstName").val(),
            lastName: $("#passegerSecondName").val(),
            password: $("#passegerPassword").val(),
            email: $("#passengerEmail").val(),
            nic: $("#passegerNIC").val(),
            phoneNumber: $("#passengerPhone").val(),
            comfirmPassword: $('#confirm-password').val()

        },
        success: function (data) {
            if (data == 1) {
                swal({
                    title: "Success...!",
                    text: "your Account has been created..!",
                    icon: "success",

                });
                getAllEmployee();
                clearEmployee();
                $('#exampleModal').hide(200);
                $('#exampleModal').modal('toggle');
            } else if (data == 10) {
                swal({
                    title: "Unsuccessful...!",
                    text: "Please check password again..!",
                    icon: "error",
                });

            } else {
                swal({
                    title: "Unsuccessful...!",
                    text: "Something wrong check again..!",
                    icon: "error",
                });
            }

        }
    });
}

//---------------------------update Employee-----------------------------

$('#updateEmp').click(function () {

    $.ajax({
        type: "POST",
        url: baseURL + 'updatePassenger',
        data: {
            username: $('#edituser-Name').val(),
            firstName: $('#editfirst-Name').val(),
            lastName: $('#editlast-Name').val(),
            password: $('#editpassword').val(),
            email: $('#editemail').val(),
            nic: $('#editnic').val(),
            mobile: $('#editmobile').val(),
            confirmPassword: $('#editconfirm-password').val()

        }
    }).done(function (resp) {

        if (resp == 1) {
            swal({
                title: "Success...!",
                text: "your Account has been updated..!",
                icon: "success",

            });
            getAllEmployee();
            clearEmployee2();
            $('#editDetails').hide(200);
            $('#editDetails').modal('toggle');

        } else if (resp == 10) {

            swal({
                title: "Unsuccessful...!",
                text: "Please check your password again..!",
                icon: "error",
            });

        } else {
            swal({
                title: "Unsuccessful...!",
                text: "Something wrong check again..!",
                icon: "error",
            });
        }


    });


});


//--------------clear textField----------------

function clearEmployee2() {
    $('#edituser-Name').val("");
    $('#editfirst-Name').val("");
    $('#editlast-Name').val("");
    $('#editpassword').val("");
    $('#editemail').val("");
    $('#editnic').val("");
    $('#editmobile').val("");
    $('#editconfirm-password').val("");
}


$('#removePassenger').click(function () {

});

$('#cancelPassenger').click(function () {
    $('#showCard').hide(400);
});


//-----------------update Passenger-------------------
$('#btn-submit').click(function () {
    $.ajax({
        type: "post",
        url: baseURL + 'addNewEmployee',
        data: {
            userID: $("#userId").val(),
            firstName: $("#first-Name").val(),
            lastName: $("#last-Name").val(),
            email: $("#email").val(),
            mobile: $("#mobile").val(),
            nic: $("#nic").val(),
            userName: $("#user-Name").val(),
            password: $("#password").val(),
            // comfirmPassword: $("#confirm-password").val()
        },
        success: function (data) {
            if (data != null) {
                clear();
            }
        }
    });
});


getAllEmployee();

//----------------get all Employees--------------

function getAllEmployee() {

    $('#userTable').empty();
    $.ajax({
        type: "GET",
        url: baseURL + 'getAllMembers',
        async: true,
        dataType: "json"
    }).done(function (responce) {

        for (let i in responce) {

            let test = responce[i];
            if (test[3] == "Employee") {

                let members = responce[i];
                let firstName = members[0];
                let lastName = members[1];
                let userName = members[2];
                let email = members[5];
                let nic = members[6];
                let mobile = members[7];

                let tableRow = "<tr>\n" +
                    "<td class=\"text-center\">" + firstName + "</td>\n" +
                    "<td class=\"text-center\"> " + lastName + "</td>\n" +
                    "<td class=\"text-center\">" + userName + "</td>\n" +
                    "<td class=\"text-center\">" + email + "</td>\n" +
                    "<td class=\"text-center\">" + nic + "</td>\n" +
                    "<td class=\"text-center\">" + mobile + "</td>\n" +
                    "<td class=\"text-center\">\n" +
                    "<button id=\"editBtn\" data-toggle=\"modal\"\n" +
                    "data-target=\"#editDetails\" class=\"btn btn-outline-info\">\n" +
                    "<i class=\"fas fa-user-edit\"></i>\n" +
                    "Edit\n" +
                    "</button>\n" +
                    "</td>\n" +
                    "</tr>";

                $('#userTable').append(tableRow);

            }
        }
    });
}


// get Selected rowData to pop up From
function getRowData() {

    $('#userTable').on('click', '#editBtn', function () {

        var cur_row = $(this).closest('tr');
        var col1 = cur_row.find('td:eq(0)').text();
        var col2 = cur_row.find('td:eq(1)').text();
        var col3 = cur_row.find('td:eq(2)').text();
        var col4 = cur_row.find('td:eq(3)').text();
        var col5 = cur_row.find('td:eq(4)').text();
        var col6 = cur_row.find('td:eq(5)').text();


        $('#editfirst-Name').val(col1);
        $('#editlast-Name').val(col2);
        $('#edituser-Name').val(col3);
        $('#editemail').val(col4);
        $('#editnic').val(col5);
        $('#editmobile').val(col6);

        $('#editpassword').val("");
        $('#editconfirm-password').val("");
    });

}

getRowData();

