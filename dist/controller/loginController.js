


$('#loginbtn').click(function () {
    $.ajax({
        type: "post",
        url:  baseURL +'getAllPassengers',
        async: true,
        dataType: "json",
        data: {
            username: $("#username").val(),
            password: $("#password").val(),
            // mode: $("#modes").val()
        }
    }).done(function (responce) {
        if (responce == 1) {
            window.location.href =  baseURL +'dashboard';
        } else if (responce == 2) {
            window.location.href =  baseURL +'employeeProfile';
        } else if (responce == 3) {
            window.location.href =  baseURL +'userDash';
        } else if (responce == 10) {
            swal("Login Failed", "User Name or Password incorrect", "error");
        }
    });

});

