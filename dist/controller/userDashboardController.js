// get current date
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
    dd = '0' + dd
}

if (mm < 10) {
    mm = '0' + mm
}

today = yyyy + '-' + mm + '-' + dd;

// loadAll User Compartment Reservation Details in here

function loadAllUserCompartmentReservationDetails() {

    let requestHeader = $('#requestHeader');
    let reqestDate = $('#reqestDate');
    let trainName = $('#trainName');
    let from = $('#from');
    let to = $('#to');
    let compartmentNo = $('#compartmentNo');
    let compartmentBody = $('#compartmentRequests');


    $.ajax({
        type: "GET",
        url: baseURL + 'getAllUserCompartmentReservations',
        async: true,
        dataType: "json"
    }).done(function (responce) {


        if (!(_.isEmpty(responce))) {

            compartmentBody.empty();

            for (let i in responce) {
                let details = responce[i];

                let trainNameDB = details[0];
                let reseveDateDB = details[1];
                let fromDB = details[2];
                let toDB = details[3];
                let compartmentNameDB = details[4];
                let statusDB = details[5];
                let rid = details[6];


                if (statusDB === "requesting") {

                    let compartmentRequests = "<div class=\"alert alert-success\" role=\"alert\">\n" +
                        "                                <h4 class=\"alert-heading\" id=\"requestHeader\"> <i class=\"fas fa-dna\"></i>Pending</h4>\n" +
                        "                                <div class=\"row\">\n" +
                        "                                    <p>You have been successfully request this Compartment on &nbsp;\n" +
                        "                                    <p class=\"text-dark\" style=\"font-weight: bold\" id=\"reqestDate\">" + reseveDateDB + "</p>\n" +
                        "                                    <p>.</p>&nbsp;Please be patient until your request is Accept.</p>\n" +
                        "                                </div>\n" +
                        "                                <hr>\n" +
                        "                                <div class=\"row m-2 text-secondary\">\n" +
                        "                                    <label for=\"#trainName\" class=\"text-primary\">Train Name</label>\n" +
                        "                                    <label id=\"trainName\" class=\"col-md-2\">" + trainNameDB + "</label>\n" +
                        "                                    <label for=\"#from\" class=\"text-primary\">From</label>\n" +
                        "                                    <label id=\"from\" class=\"col-md-2\">" + fromDB + "</label>\n" +
                        "                                    <label for=\"#to\" class=\"text-primary\">To</label>\n" +
                        "                                    <label id=\"to\" class=\"col-md-2\">" + toDB + "</label>\n" +
                        "                                    <label for=\"#compartmentNo\" class=\"text-primary\">Compartment No</label>\n" +
                        "                                    <label id=\"compartmentNo\" class=\"col-md-2\">" + compartmentNameDB + "</label>\n" +
                        "                                    <button id=\"" + rid + "\" class=\"btn btn-outline-danger \" onclick=\"cancelCompartmentRequest(this.id);\">Cancel</button>\n" +
                        "                                </div>\n" +
                        "                                <hr>\n" +
                        "                                <!--                                <p class=\"mb-0\">Whenever you need to, be sure to use margin utilities to keep things-->\n" +
                        "                                <!--                                    nice-->\n" +
                        "                                <!--                                    and tidy.</p>-->\n" +
                        "                            </div>\n" +
                        "                        </div>";


                    compartmentBody.append(compartmentRequests);
                }

                if (statusDB === "Request Canceled") {

                    let compartmentCancelRequests = "<div class=\"alert alert-secondary\" role=\"alert\">\n" +
                        "                                <h4 class=\"alert-heading\" id=\"requestHeader\"> <i class=\"fas fa-dna\"></i>Canceled</h4>\n" +
                        "                                <div class=\"row\">\n" +
                        "                                    <p>You have been successfully cancel your Compartment request on &nbsp;\n" +
                        "                                    <p class=\"text-dark\" style=\"font-weight: bold\" id=\"reqestDate\">" + reseveDateDB + "</p>\n" +
                        "                                </div>\n" +
                        "                                <hr>\n" +
                        "                                <div class=\"row m-2 text-secondary\">\n" +
                        "                                    <label for=\"#trainName\" class=\"text-primary\">Train Name</label>\n" +
                        "                                    <label id=\"trainName\" class=\"col-md-2\">" + trainNameDB + "</label>\n" +
                        "                                    <label for=\"#from\" class=\"text-primary\">From</label>\n" +
                        "                                    <label id=\"from\" class=\"col-md-2\">" + fromDB + "</label>\n" +
                        "                                    <label for=\"#to\" class=\"text-primary\">To</label>\n" +
                        "                                    <label id=\"to\" class=\"col-md-2\">" + toDB + "</label>\n" +
                        "                                    <label for=\"#compartmentNo\" class=\"text-primary\">Compartment No</label>\n" +
                        "                                    <label id=\"compartmentNo\" class=\"col-md-2\">" + compartmentNameDB + "</label>\n" +
                        "                                    <button id=\"" + rid + "\" class=\"btn btn-outline-danger \" onclick=\"cancelCompartmentRequest(this.id);\" disabled>Canceled</button>\n" +
                        "                                </div>\n" +
                        "                                <hr>\n" +
                        "                                <!--                                <p class=\"mb-0\">Whenever you need to, be sure to use margin utilities to keep things-->\n" +
                        "                                <!--                                    nice-->\n" +
                        "                                <!--                                    and tidy.</p>-->\n" +
                        "                            </div>\n" +
                        "                        </div>";


                    compartmentBody.append(compartmentCancelRequests);
                }
                if (statusDB === "Booked") {

                    let compartmentCancelRequests = "<div class=\"alert alert-primary\" role=\"alert\">\n" +
                        "                                <h4 class=\"alert-heading\" id=\"requestHeader\"><i class=\"far fa-thumbs-up\"></i>Request Confirmed</h4>\n" +
                        "                                <div class=\"row\">\n" +
                        "                                    <p>Your Compartment Booking request on &nbsp;\n" +
                        "                                    <p class=\"text-dark\" style=\"font-weight: bold\" id=\"reqestDate\">" + reseveDateDB + "</p>\n" +
                        "        <p>.</p>&nbsp;Accepted by ADMIN.</p>\n" +
                        "                                </div>\n" +
                        "                                <hr>\n" +
                        "                                <div class=\"row m-2 text-secondary\">\n" +
                        "                                    <label for=\"#trainName\" class=\"text-primary\">Train Name</label>\n" +
                        "                                    <label id=\"trainName\" class=\"col-md-2\">" + trainNameDB + "</label>\n" +
                        "                                    <label for=\"#from\" class=\"text-primary\">From</label>\n" +
                        "                                    <label id=\"from\" class=\"col-md-2\">" + fromDB + "</label>\n" +
                        "                                    <label for=\"#to\" class=\"text-primary\">To</label>\n" +
                        "                                    <label id=\"to\" class=\"col-md-2\">" + toDB + "</label>\n" +
                        "                                    <label for=\"#compartmentNo\" class=\"text-primary\">Compartment No</label>\n" +
                        "                                    <label id=\"compartmentNo\" class=\"col-md-2\">" + compartmentNameDB + "</label>\n" +
                        "<img src=\"../dist/img/accept.png\" style=\"max-height:8vh ;max-width: 5vw\">\n" +
                        "                                </div>\n" +
                        "                                <hr>\n" +
                        "                                <!--                                <p class=\"mb-0\">Whenever you need to, be sure to use margin utilities to keep things-->\n" +
                        "                                <!--                                    nice-->\n" +
                        "                                <!--                                    and tidy.</p>-->\n" +
                        "                            </div>\n" +
                        "                        </div>";


                    compartmentBody.append(compartmentCancelRequests);
                }


                if (statusDB === "Request Rejected") {

                    let compartmentRejectRequests = "<div class=\"alert alert-danger\" role=\"alert\">\n" +
                        "                                <h4 class=\"alert-heading\" id=\"requestHeader\"><i class=\"far fa-thumbs-down\"></i>Request Rejected</h4>\n" +
                        "                                <div class=\"row\">\n" +
                        "                                    <p>Your Compartment Booking request on &nbsp;\n" +
                        "                                    <p class=\"text-dark\" style=\"font-weight: bold\" id=\"reqestDate\">" + reseveDateDB + "</p>\n" +
                        "        <p>.</p>&nbsp;Rejected by ADMIN.</p>\n" +
                        "                                </div>\n" +
                        "                                <hr>\n" +
                        "                                <div class=\"row m-2 text-secondary\">\n" +
                        "                                    <label for=\"#trainName\" class=\"text-primary\">Train Name</label>\n" +
                        "                                    <label id=\"trainName\" class=\"col-md-2\">" + trainNameDB + "</label>\n" +
                        "                                    <label for=\"#from\" class=\"text-primary\">From</label>\n" +
                        "                                    <label id=\"from\" class=\"col-md-2\">" + fromDB + "</label>\n" +
                        "                                    <label for=\"#to\" class=\"text-primary\">To</label>\n" +
                        "                                    <label id=\"to\" class=\"col-md-2\">" + toDB + "</label>\n" +
                        "                                    <label for=\"#compartmentNo\" class=\"text-primary\">Compartment No</label>\n" +
                        "                                    <label id=\"compartmentNo\" class=\"col-md-2\">" + compartmentNameDB + "</label>\n" +
                        "<img src=\"../dist/img/reject.jpg\" style=\"max-height:8vh ;max-width: 5vw\">\n" +
                        "                                </div>\n" +
                        "                                <hr>\n" +
                        "                                <!--                                <p class=\"mb-0\">Whenever you need to, be sure to use margin utilities to keep things-->\n" +
                        "                                <!--                                    nice-->\n" +
                        "                                <!--                                    and tidy.</p>-->\n" +
                        "                            </div>\n" +
                        "                        </div>";


                    compartmentBody.append(compartmentRejectRequests);
                }

            }
        } else {
            console.log("No current Compartment Requests");


        }
    });
}

function cancelCompartmentRequest(reservationID) {


    console.log(reservationID);
    console.log(today); // line 202
    $.ajax({
        type: "POST",
        url: baseURL + 'cancelCompartmentRequest',
        async: true,
        dataType: "json",
        data: {
            reservationID: reservationID,
            cur_date: '2019-02-27'  //set today its locate in today variable
        }
    }).done(function (responce) {


        if (responce == 1) {
            swal({
                title: "Success...!",
                text: "Your Request has been cancel Successfully..!",
                icon: "success",
            });
            document.location.reload();
        }
        else if (responce == 10) {
            swal({
                title: "Oops...!",
                text: "You cannot cancel this request that's today..!",
                icon: "error",
            });
        }
    });
}

loadAllUserCompartmentReservationDetails();

// // loadAll User Train Reservation Details in here

function loadAllUserTrainReservationDetails() {


    let trainRequestsBody = $('#trainRequests');

    $.ajax({
        type: "GET",
        url: baseURL + 'getAllUserTrainReservations',
        async: true,
        dataType: "json"
    }).done(function (responce) {
        console.log(_.isEmpty(responce));
        if (!(_.isEmpty(responce))) {

            trainRequestsBody.empty();
            for (let i in responce) {
                let details = responce[i];

                let trainName = details[0];
                let reseveDate = details[1];
                let from = details[2];
                let to = details[3];
                let className = details[4];
                let status = details[5];
                let rid = details[6];

                if (status === "request") {
                    let trainRequets = "<div class=\"alert alert-success\" role=\"alert\">\n" +
                        "    <h4 class=\"alert-heading\" id=\"requestHeader\"><i class=\"fas fa-dna\"></i>Pending</h4>\n" +
                        "    <div class=\"row\">\n" +
                        "        <p>You have been successfully request this Train on &nbsp;\n" +
                        "        <p class=\"text-dark\" style=\"font-weight: bold\" id=\"reqestDate\">" + reseveDate + "</p>\n" +
                        "        <p>.</p>&nbsp;Please be patient until your request is Accept.</p>\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "    <div class=\"row m-2 text-secondary\">\n" +
                        "        <label for=\"#trainName\" class=\"text-primary\">Train Name</label>\n" +
                        "        <label id=\"trainName\" class=\"col-md-2\">" + trainName + "</label>\n" +
                        "        <label for=\"#from\" class=\"text-primary\">From</label>\n" +
                        "        <label id=\"from\" class=\"col-md-2\">" + from + "</label>\n" +
                        "        <label for=\"#to\" class=\"text-primary\">To</label>\n" +
                        "        <label id=\"to\" class=\"col-md-2\">" + to + "</label>\n" +
                        "<label for=\"#classNo\" class=\"text-primary\">ClassNo</label>\n" +
                        "<label id=\"classNo\" class=\"col-md-2\">" + className + "</label>\n" +
                        "        <button id=\"" + rid + "\" class=\"btn btn-outline-danger \" " +
                        "onclick=\"cancelTrainRequest(this.id);\"><i class=\"fas fa-subway\"></i>Cancel</button>\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "</div>\n" +
                        "</div>";


                    trainRequestsBody.append(trainRequets);
                }
                if (status === "Request Canceled") {

                    let trainCanceledRequets = "<div class=\"alert alert-secondary\" role=\"alert\">\n" +
                        "    <h4 class=\"alert-heading\" id=\"requestHeader\"><i class=\"far fa-bell-slash\"></i>Canceled</h4>\n" +
                        "    <div class=\"row\">\n" +
                        "        <p>You have been successfully cancel your request this Train on &nbsp;\n" +
                        "        <p class=\"text-dark\" style=\"font-weight: bold\" id=\"reqestDate\">" + reseveDate + "</p>\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "    <div class=\"row m-2 text-secondary\">\n" +
                        "        <label for=\"#trainName\" class=\"text-primary\">Train Name</label>\n" +
                        "        <label id=\"trainName\" class=\"col-md-2\">" + trainName + "</label>\n" +
                        "        <label for=\"#from\" class=\"text-primary\">From</label>\n" +
                        "        <label id=\"from\" class=\"col-md-2\">" + from + "</label>\n" +
                        "        <label for=\"#to\" class=\"text-primary\">To</label>\n" +
                        "        <label id=\"to\" class=\"col-md-2\">" + to + "</label>\n" +
                        "<label for=\"#classNo\" class=\"text-primary\">ClassNo</label>\n" +
                        "<label id=\"classNo\" class=\"col-md-2\">" + className + "</label>\n" +
                        "        <button id=\"" + rid + "\" class=\"btn btn-outline-danger \" " +
                        "onclick=\"cancelTrainRequest(this.id);\" disabled><i class=\"fas fa-subway\"></i>Canceled</button>\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "</div>\n" +
                        "</div>";
                    trainRequestsBody.append(trainCanceledRequets);
                }
                if (status === "Booked") {

                    let trainBookedRequets = "<div class=\"alert alert-primary\" role=\"alert\">\n" +
                        "    <h4 class=\"alert-heading\" id=\"requestHeader\"><i class=\"far fa-thumbs-up\"></i>Request Confirm</h4>\n" +
                        "    <div class=\"row\">\n" +
                        "        <p>Your Train Booking request is on &nbsp;\n" +
                        "        <p class=\"text-dark\" style=\"font-weight: bold\" id=\"reqestDate\">" + reseveDate + "</p>\n" +
                        "        <p>.</p>&nbsp;Accepted by ADMIN.</p>\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "    <div class=\"row m-2 text-secondary\">\n" +
                        "        <label for=\"#trainName\" class=\"text-primary\">Train Name</label>\n" +
                        "        <label id=\"trainName\" class=\"col-md-2\">" + trainName + "</label>\n" +
                        "        <label for=\"#from\" class=\"text-primary\">From</label>\n" +
                        "        <label id=\"from\" class=\"col-md-2\">" + from + "</label>\n" +
                        "        <label for=\"#to\" class=\"text-primary\">To</label>\n" +
                        "        <label id=\"to\" class=\"col-md-2\">" + to + "</label>\n" +
                        "<label for=\"#classNo\" class=\"text-primary\">ClassNo</label>\n" +
                        "<label id=\"classNo\" class=\"col-md-2\">" + className + "</label>\n" +
                        // "        <button id=\"" + rid + "\" class=\"btn btn-outline-danger \" " +
                        // "onclick=\"cancelTrainRequest(this.id);\" disabled><i class=\"fas fa-subway\"></i>Canceled</button>\n" +
                        "<img src=\"../dist/img/accept.png\" style=\"max-height:8vh ;max-width: 8vw\">\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "</div>\n" +
                        "</div>";
                    trainRequestsBody.append(trainBookedRequets);
                }

                if (status === "Request Rejected") {

                    let trainBookedRequets = "<div class=\"alert alert-danger\" role=\"alert\">\n" +
                        "    <h4 class=\"alert-heading\" id=\"requestHeader\"><i class=\"far fa-thumbs-down\"></i>Request Rejected</h4>\n" +
                        "    <div class=\"row\">\n" +
                        "        <p>Your Train Booking request is on &nbsp;\n" +
                        "        <p class=\"text-dark\" style=\"font-weight: bold\" id=\"reqestDate\">" + reseveDate + "</p>\n" +
                        "        <p>.</p>&nbsp;Rejected by ADMIN.</p>\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "    <div class=\"row m-2 text-secondary\">\n" +
                        "        <label for=\"#trainName\" class=\"text-primary\">Train Name</label>\n" +
                        "        <label id=\"trainName\" class=\"col-md-2\">" + trainName + "</label>\n" +
                        "        <label for=\"#from\" class=\"text-primary\">From</label>\n" +
                        "        <label id=\"from\" class=\"col-md-2\">" + from + "</label>\n" +
                        "        <label for=\"#to\" class=\"text-primary\">To</label>\n" +
                        "        <label id=\"to\" class=\"col-md-2\">" + to + "</label>\n" +
                        "<label for=\"#classNo\" class=\"text-primary\">ClassNo</label>\n" +
                        "<label id=\"classNo\" class=\"col-md-2\">" + className + "</label>\n" +
                        // "        <button id=\"" + rid + "\" class=\"btn btn-outline-danger \" " +
                        // "onclick=\"cancelTrainRequest(this.id);\" disabled><i class=\"fas fa-subway\"></i>Canceled</button>\n" +
                        "<img src=\"../dist/img/reject.jpg\" style=\"max-height:8vh ;max-width: 10vw\">\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "</div>\n" +
                        "</div>";
                    trainRequestsBody.append(trainBookedRequets);
                }

            }
        } else {
            console.log("No current Train Requests");
        }
    });
}

loadAllUserTrainReservationDetails();

function cancelTrainRequest(reservationID) {

    console.log(reservationID);
    $.ajax({
        type: "POST",
        url: baseURL + 'cancelTrainRequest',
        async: true,
        dataType: "json",
        data: {
            reservationID: reservationID,
            cur_date: '2019-02-26'  //set today its locate in today variable
        }
    }).done(function (responce) {


        if (responce == 1) {
            swal({
                title: "Success...!",
                text: "Your request has been cancel successfully..!",
                icon: "success",
            });
            document.location.reload();
        }
        else if (responce == 10) {
            swal({
                title: " Oops...!",
                text: "You cannot cancel this request that's today..!",
                icon: "error",
            });
        }
    });
}


function loadAllUserSeatBookingDetails() {


    let seatReservationBody = $('#seatbookings');

    $.ajax({
        type: "GET",
        url: baseURL + 'getAllUserSeatReservations',
        async: true,
        dataType: "json"
    }).done(function (responce) {
        console.log(_.isEmpty(responce));
        if (!(_.isEmpty(responce))) {

            seatReservationBody.empty();
            for (let i in responce) {
                let details = responce[i];

                let trainName = details[0];
                let reseveDate = details[1];
                let to = details[2];
                let from = details[3];
                let compartmantNo = details[4];
                let seatNo = details[5];
                let rid = details[6];

                if (reseveDate >= today) {
                    let trainRequets = "<div class=\"alert alert-success\" role=\"alert\">\n" +
                        "    <h4 class=\"alert-heading\" id=\"requestHeader\"><i class=\"fas fa-dna\"></i>Pending</h4>\n" +
                        "    <div class=\"row\">\n" +
                        "        <p>You have been successfully booked tickets on this Train on &nbsp;\n" +
                        "        <p class=\"text-dark\" style=\"font-weight: bold\" id=\"reqestDate\">" + reseveDate + "</p>\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "    <div class=\"row m-2 text-secondary\">\n" +
                        "        <label for=\"#trainName\" class=\"text-primary\">Train</label>\n" +
                        "        <label id=\"trainName\" class=\"col-md-2\">" + trainName + "</label>\n" +
                        "        <label for=\"#from\" class=\"text-primary\">From</label>\n" +
                        "        <label id=\"from\" class=\"col-md-2\">" + from + "</label>\n" +
                        "        <label for=\"#to\" class=\"text-primary\">To</label>\n" +
                        "        <label id=\"to\" class=\"col-md-2\">" + to + "</label>\n" +
                        "<label for=\"#classNo\" class=\"text-primary\">COM</label>\n" +
                        "<label id=\"classNo\" class=\"col-md-2\">" + compartmantNo + "</label>\n"+
                        "<label for=\"#classNo\" class=\"text-primary\">SeatNO</label>\n" +
                        "<label id=\"classNo\" class=\"col-md-1\">" + seatNo + "</label>\n" +
                        "        <button id=\"" + rid + "\" class=\"btn btn-outline-danger \" " +
                        "onclick=\"cancelSeatRequest(this.id);\"><i class=\"fas fa-subway\"></i>Cancel</button>\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "</div>\n" +
                        "</div>";


                    seatReservationBody.append(trainRequets);
                }
                if (reseveDate < today) {

                    let trainCanceledRequets = "<div class=\"alert alert-secondary\" role=\"alert\">\n" +
                        "    <h4 class=\"alert-heading\" id=\"requestHeader\"><i class=\"far fa-bell-slash\"></i>Expired</h4>\n" +
                        "    <div class=\"row\">\n" +
                        "        <p>Your has been expired your booking on this Train on &nbsp;\n" +
                        "        <p class=\"text-dark\" style=\"font-weight: bold\" id=\"reqestDate\">" + reseveDate + "</p>\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "    <div class=\"row m-2 text-secondary\">\n" +
                        "        <label for=\"#trainName\" class=\"text-primary\">Train</label>\n" +
                        "        <label id=\"trainName\" class=\"col-md-2\">" + trainName + "</label>\n" +
                        "        <label for=\"#from\" class=\"text-primary\">From</label>\n" +
                        "        <label id=\"from\" class=\"col-md-2\">" + from + "</label>\n" +
                        "        <label for=\"#to\" class=\"text-primary\">To</label>\n" +
                        "        <label id=\"to\" class=\"col-md-2\">" + to + "</label>\n" +
                        "<label for=\"#classNo\" class=\"text-primary\">COM</label>\n" +
                        "<label id=\"classNo\" class=\"col-md-2\">" + compartmantNo + "</label>\n" +
                        "<label for=\"#classNo\" class=\"text-primary\">SeatNO</label>\n" +
                        "<label id=\"classNo\" class=\"col-md-1\">" + seatNo + "</label>\n" +
                        // "        <button id=\"" + rid + "\" class=\"btn btn-outline-danger \" " +
                        // "onclick=\"cancelSeatRequest(this.id);\" disabled><i class=\"fas fa-subway\"></i></button>\n" +
                        "    </div>\n" +
                        "    <hr>\n" +
                        "</div>\n" +
                        "</div>";
                    seatReservationBody.append(trainCanceledRequets);
                }

            }
        } else {
            console.log("No current Train Requests");
        }
    });
}

loadAllUserSeatBookingDetails();

function cancelSeatRequest(reservationID) {

    console.log(reservationID);
    $.ajax({
        type: "POST",
        url: baseURL + 'cancelThisReservation',
        async: true,
        dataType: "json",
        data: {
            reservationID: reservationID,
            // cur_date: '2019-02-26'  //set today its locate in today variable
        }
    }).done(function (responce) {


        if (responce == 1) {
            swal({
                title: "Success...!",
                text: "Your request has been cancel successfully..!",
                icon: "success",
            });
            document.location.reload();
        }
        else if (responce == 10) {
            swal({
                title: " Oops...!",
                text: "You cannot cancel this request that's today..!",
                icon: "error",
            });
        }
    });
}
