//-----------update a passenger---------------------
$('#updateModalPassenger').click(function () {

    let edituserName = $('#modalUsername').val();
    let editFirstName = $('#modalFirstName').val();
    let editLastName = $('#modalLastName').val();
    let editPassword = $('#password').val();
    let editEmail = $('#modalEmail').val();
    let editNic = $('#modalNic').val();
    let editMobile = $('#modalPhoneNumber').val();
    let confirmPassword = $('#modalconfirmpassword').val();

    if (editPassword === confirmPassword) {
        $.ajax({
            type: "POST",
            url: baseURL + 'updatePassengerFromPassenger',
            async: true,
            data: {
                username: edituserName,
                firstName: editFirstName,
                lastName: editLastName,
                password: editPassword,
                email: editEmail,
                nic: editNic,
                mobile: editMobile,

            },
        }).done(function (resp) {

            if (resp == 1) {
                swal({
                    title: "Success...!",
                    text: "Your Account Has Been Updated..!",
                    icon: "success",

                });
                getAllPassenger();
                userProfile();
                $('#editDetails').hide(200);
                $('#editDetails').modal('toggle');
            } else {
                swal({
                    title: " Can't Update...!",
                    text: "Something Wrong Check Again..!",
                    icon: "error",
                });
                clear2();
            }
        });

    } else {
        swal({
            title: "Can't Update...!",
            text: "Password is not matching..!",
            icon: "error",
        });
    }


});

//---------------------userProfile-----------------------------

$(document).ready(function () {
    getRowData();
});

getRowData();

// get Selected rowData to pop up From

function getRowData() {
    $('#userTable').on('click', '#editBtn', function () {
        var cur_row = $(this).closest('tr');
        var col1 = cur_row.find('td:eq(0)').text();
        var col2 = cur_row.find('td:eq(1)').text();
        var col3 = cur_row.find('td:eq(2)').text();
        var col4 = cur_row.find('td:eq(3)').text();
        var col5 = cur_row.find('td:eq(4)').text();
        var col6 = cur_row.find('td:eq(5)').text();


        $('#editfirst-Name').val(col1);
        $('#editlast-Name').val(col2);
        $('#edituser-Name').val(col3);
        $('#editemail').val(col4);
        $('#editnic').val(col5);
        $('#editmobile').val(col6);

        $('#editpassword').val("");
        $('#editconfirm-password').val("");
    });
}

//------call the method----------------------------

getAllPassenger();
//----------------------get All Passenger---------------------

var rowData = new Array();

function getAllPassenger() {

    $('#userTable').empty();
    $.ajax({
        type: "GET",
        url: baseURL + 'getAllMembers',
        async: true,
        dataType: "json"
    }).done(function (responce) {

        for (let i in responce) {

            let test = responce[i];
            if (test[3] == "Passenger") {

                let members = responce[i];
                let firstName = members[0];
                let lastName = members[1];
                let userName = members[2];
                let email = members[5];
                let nic = members[6];
                let mobile = members[7];

                let tableRow = "<tr>\n" +
                    "<td class=\"text-center\">" + firstName + "</td>\n" +
                    "<td class=\"text-center\"> " + lastName + "</td>\n" +
                    "<td class=\"text-center\">" + userName + "</td>\n" +
                    "<td class=\"text-center\">" + email + "</td>\n" +
                    "<td class=\"text-center\">" + nic + "</td>\n" +
                    "<td class=\"text-center\">" + mobile + "</td>\n" +
                    "<td class=\"text-center\">\n" +
                    "<button id=\"editBtn\" data-toggle=\"modal\"\n" +
                    "data-target=\"#editDetails\" class=\"btn btn-outline-info\" style=\"border-radius: 16px;!important;\">\n" +
                    "<i class=\"fas fa-user-edit\"></i>\n" +
                    "Edit\n" +
                    "</button>\n" +
                    "</td>\n" +
                    "</tr>";

                $('#userTable').append(tableRow);

            }
        }
    });
}
