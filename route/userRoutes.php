<?php
//require_once __DIR__ . "/../api/bo/Impl/PassengerBOImpl.php";
//require_once __DIR__ . "/../api/core/Passenger.php";
//
//$app->get('/loginpage', function ($request, $response) {
//
//    session_unset();
//    session_destroy();
//
//    $response = $this->view->render($response, 'login.php');
//    return $response;
//});
$app->get('/userDash', function ($request, $response) {
    if (!isset($_SESSION["user"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;
    } else {
        $response = $this->view->render($response, 'userDashBoard.php');
        return $response;
    }
});

$app->get('/profile', function ($request, $response) {
    if (!isset($_SESSION["user"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;
    } else {
        $response = $this->view->render($response, 'userProfile1.php');
        return $response;
    }
});
$app->get('/retireroom-reservation', function ($request, $response) {
    if (!isset($_SESSION["user"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;
    } else {

        $response = $this->view->render($response, 'retire-roomReservation.php');
        return $response;
    }
});
