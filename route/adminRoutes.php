<?php

require_once __DIR__ . "/../api/bo/Impl/TrainReservationBOImpl.php";
require_once __DIR__ . "/../api/bo/Impl/SeatReservationBOImpl.php";
require_once __DIR__ . "/../api/bo/Impl/CompartmentReservationBOImpl.php";
require_once __DIR__ . "/../api/bo/Impl/StationBOImpl.php";

$app->get('/manageEmployee', function ($request, $response) {
    if(!isset($_SESSION["admin"])){
        $response = $this->view->render($response, 'login.php');
        return $response;
    }else {
        $response = $this->view->render($response, 'manageEmployee.php');
        return $response;
    }
});

$app->get('/manageRevenue', function ($request, $response) {
    if(!isset($_SESSION["admin"])){
        $response = $this->view->render($response, 'login.php');
        return $response;
    }else {
        $response = $this->view->render($response, 'revenueManagement.php');
        return $response;
    }
});


$app->get('/seatReservation', function ($request, $response) {
    if(!isset($_SESSION["admin"])){
        $response = $this->view->render($response, 'login.php');
        return $response;
    }else {

        $response = $this->view->render($response, 'seatReservation.php');
        return $response;
    }
});
$app->get('/payment', function ($request, $response) {
    if(!isset($_SESSION["admin"])){
        $response = $this->view->render($response, 'login.php');
        return $response;
    }else {
        $response = $this->view->render($response, 'payment.php');
        return $response;
    }
});

$app->get('/admintrainreservation', function ($request, $response) {
    if(!isset($_SESSION["admin"])){
        $response = $this->view->render($response, 'login.php');
        return $response;
    }else {

        $response = $this->view->render($response, 'trainReservationDetail.php');
        return $response;
    }
});

$app->get('/compartmentreservation', function ($request, $response) {
    if(!isset($_SESSION["admin"])){
        $response = $this->view->render($response, 'login.php');
        return $response;
    }else {

        $response = $this->view->render($response, 'compartmentReservation.php');
        return $response;
    }
});
$app->get('/trainReservation', function ($request, $response) {
    if (!isset($_SESSION["admin"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {
        $response = $this->view->render($response, 'trainReservationAllDetail.php');
        return $response;
    }

});
$app->get('/compartmentReservation', function ($request, $response) {
    if (!isset($_SESSION["admin"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {
        $response = $this->view->render($response, 'compartmentReservertionDetail.php');
        return $response;
    }
});

$app->get('/getAllTrainReservationDetails', function ($request, $response) {

    $trainRequests = new TrainReservationBOImpl();
    echo json_encode($trainRequests->getAllTrainReservations());
});

$app->get('/getAllSeatReservations', function ($request, $response) {
    $seatReservationRepo = new SeatReservationBOImpl();
    echo json_encode($seatReservationRepo->getAllSeatReservation());
});

$app->get('/getAllCompartmentRequest', function ($request, $response) {
    $compartmentReservationRepo = new CompartmentReservationBOImpl();
    echo json_encode($compartmentReservationRepo->getAllRequestingCompartmentReservations());
});

$app->get('/getAllTrainRequest', function ($request, $response) {
    $trainReservationRepo = new TrainReservationBOImpl();
    echo json_encode($trainReservationRepo->getAllTrainRequestReservations());
});

$app->get('/getAllTrainRequestReservationDetail', function ($request, $response) {
    $trainReservationRepo = new TrainReservationBOImpl();
    echo json_encode($trainReservationRepo->getAllTrainRequestReservationDetails());
});

$app->get('/getAllCompartmentAllReservationDetail', function ($request, $response) {
    $compartReservationRepo = new CompartmentReservationBOImpl();
    echo json_encode($compartReservationRepo->getAllCompartmentAllReservations());
});

$app->get('/getAllSchedule', function ($request, $response) {

    $scheduleBO = new SheduleBOImpl();
    echo json_encode($scheduleBO->getAllShedule());
});

$app->post('/acceptingTrainRequest', function ($request, $response) {

    $json = $request->getParsedBody();

    $reservationID = $json["rid"];

    $trainReservationBo = new TrainReservationBOImpl();
    $result=$trainReservationBo->acceptingTrainRequest($reservationID);
    echo $result;
});

$app->post('/rejectingTrainRequest', function ($request, $response) {

    $json = $request->getParsedBody();

    $reservationID = $json["rid"];

    $trainReservationBo = new TrainReservationBOImpl();
    $result=$trainReservationBo->rejectingTrainRequest($reservationID);
    echo $result;
});

$app->post('/acceptingCompartmentRequest', function ($request, $response) {

    $json = $request->getParsedBody();

    $reservationID = $json["rid"];

    $trainReservationBo = new CompartmentReservationBOImpl();
    $result=$trainReservationBo->acceptingCompartmentRequest($reservationID);
    echo $result;
});

$app->post('/rejectingCompartmentRequest', function ($request, $response) {

    $json = $request->getParsedBody();

    $reservationID = $json["rid"];

    $trainReservationBo = new CompartmentReservationBOImpl();
    $result=$trainReservationBo->rejectingCompartmentRequest($reservationID);
    echo $result;

});

$app->post('/getUserDetailsToSendMail', function ($request, $response) {

    $json = $request->getParsedBody();

    $reservationID= $json["rid"];
    $compartmentBo = new CompartmentReservationBOImpl();
    $result = $compartmentBo->getUserReservationDetailsToSendMail($reservationID);
    echo json_encode($result);
});