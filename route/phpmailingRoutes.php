<?php
require "../api/common/PhpMailSystem.php";

$app->post('/MailingSystem', function ($request,$response){

    $phpMailing=new PhpMailSystem();
    $json = $request-> getParsedBody();

    $email = $json['email'];
    $task = $json['reservationName'];
    $date = $json['date'];

    echo $phpMailing->sendMailForCustomer($email,$task,$date);

});
$app->post('/adminAcceptingAndRejectingReservations', function ($request,$response){

    $phpMailing=new PhpMailSystem();
    $json = $request-> getParsedBody();

    $email = $json['email'];
    $task = $json['reservationName'];
    $date = $json['date'];
    $status = $json['status'];

    echo $phpMailing->sendMailForPassengerReservation($email,$task,$date,$status);

});
