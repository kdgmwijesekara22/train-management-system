<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dhanush
* Date: 2/14/2019
* Time: 5:35 PM
*/

require __DIR__ . "/../api/bo/Impl/StationBOImpl.php";


$app->get('/searchStation', function ($request,$response){

    $json = $request->getParsedBody();
    $stationID = $json["stationID"];
    $manageStationBO = new StationBOImpl();
    echo json_encode($manageStationBO->searchByID($stationID));

});
