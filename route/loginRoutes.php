<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dhanush
 * Date: 2/6/2019
 * Time: 1:23 PM
 */

use api\db\DBConnection;

$app->get('/', function ($request, $response) {
    $response = $this->view->render($response, 'user-main.php');
    return $response;

});

$app->post('/getAllPassengers', function ($request, $response) {
    $json = $request->getParsedBody();

    $username = $json["username"];
    $password = $json["password"];
//    $type = $json["mode"];
    $con = (new DBConnection())->getDBConnection();
    $query = "SELECT _user.types FROM _user WHERE _user.userName = '{$username}' && _user.password = '{$password}'";
    $result = mysqli_query($con, $query);

    if (mysqli_num_rows($result) == 1) {
        $row = $result->fetch_assoc();
        $dbType = $row['types'];
        if (!isset($_SESSION["user"]) & !isset($_SESSION["Admin"]) & !isset($_SESSION["Employee"])) {

            if ($dbType === 'Admin') {
                $_SESSION["admin"] = $username;
                $_SESSION["user"] = $username;
//                $_SESSION["userName"]=$username;
//                $_SESSION["userType"]="ADMIN";
                echo "1";
            } else if ($dbType === 'Employee') {
                $_SESSION["employee"] = $username;
                $_SESSION["user"] = $username;
//                $_SESSION["userName"]=$username;
//                $_SESSION["userType"]="EMPLOYEE";
                echo "2";
            } else if ($dbType === 'Passenger') {
                $_SESSION["user"] = $username;
//                $_SESSION["userName"]=$username;
//                $_SESSION["userType"]="PASSENGER";
                echo "3";
            } else {
                echo "4";
            }
        }
    } else {
        echo "10";
    }
});

$app->get('/log-out', function ($request, $response) {

    if(isset($_SESSION['admin'])){
        session_unset();
        session_destroy();
        echo 1;
    }
    if(isset($_SESSION['employee'])){
        session_unset();
        session_destroy();
        echo 2;
    }
    if(isset($_SESSION['user'])){
        session_unset();
        session_destroy();
        echo 3;
    }

});