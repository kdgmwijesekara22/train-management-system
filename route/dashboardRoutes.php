<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dhanush
 * Date: 2/6/2019
 * Time: 2:34 PM
 */


$app->get('/dashboard', function ($request,$response){
    //echo '<h1>LOGIN</h1>';

    if(!isset($_SESSION["admin"])){
        $response = $this->view->render($response, 'login.php');
        return $response;
    }else {

        $response = $this->view->render($response, 'admin-dashboard.php');
        return $response;
    }
});