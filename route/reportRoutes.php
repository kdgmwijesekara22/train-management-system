<?php

$app->get('/report', function ($request,$response){

    if(!isset($_SESSION["admin"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;
    }else {
        $response = $this->view->render($response, 'reports.php');
        return $response;
    }
});