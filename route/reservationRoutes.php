<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dhanush
 * Date: 2/7/2019
 * Time: 10:36 AM
 */

require_once __DIR__ . "/../api/bo/Impl/SheduleBOImpl.php";
require_once __DIR__ . "/../api/bo/Impl/TrainBOImpl.php";
require_once __DIR__ . "/../api/bo/Impl/StationBOImpl.php";


$app->get('/reservation', function ($request, $response) {
    if(!isset($_SESSION["admin"])){
        $response = $this->view->render($response, 'login.php');
        return $response;
    }else {

        $response = $this->view->render($response, 'reservation.php');
        return $response;
    }
});


//---------------method----------------

$app->get('/getAllShedule', function ($request, $response) {

    $sheduleBO = new SheduleBOImpl();
    echo json_encode($sheduleBO->getAllShedule());
});

$app->get('/getAllTrain', function ($request, $response) {

    $trainBO = new TrainBOImpl();
    echo json_encode($trainBO->getAllTrains());
});

$app->get('/getAllStation', function ($request, $response) {

    $stationBO = new StationBOImpl();
    echo json_encode($stationBO->getAllStation());
});
