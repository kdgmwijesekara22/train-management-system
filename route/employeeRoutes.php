<?php

//require  "../../api/bo/impl/ManageEmployeeBOImpl.php";
require_once __DIR__ . "/../api/bo/Impl/ManageEmployeeBOImpl.php";
require_once __DIR__ . "/../api/bo/Impl/TrainReservationBOImpl.php";
require_once __DIR__ . "/../api/bo/Impl/SeatBookingReservationBOImpl.php";
require_once __DIR__ . "/../api/bo/Impl/CompartmentReservationBOImpl.php";
require_once __DIR__ . "/../api/bo/Impl/SeatReservationBOImpl.php";
require_once __DIR__ . "/../api/bo/Impl/CompartmentBOImpl.php";

$app->get('/employeeDash', function ($request, $response) {

    if (!isset($_SESSION["employee"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {
        $response = $this->view->render($response, 'employeeSeatBookingFrom.php');
        return $response;
    }
});

$app->get('/employeManageUser', function ($request, $response) {

    if (!isset($_SESSION["employee"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {
        $response = $this->view->render($response, 'emplyeeManageUser.php');
        return $response;
    }
});

$app->get('/ticketBooking', function ($request, $response) {

    if (!isset($_SESSION["employee"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {
        $response = $this->view->render($response, '');
        return $response;
    }
});

$app->get('/EmployeeSeatBooking', function ($request, $response) {

    if (!isset($_SESSION["employee"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {
        $response = $this->view->render($response, 'employeeSeatBookingFrom.php');
        return $response;
    }
});
$app->get('/emp-compartment-reservation', function ($request, $response) {
    if (!isset($_SESSION["employee"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {

        $response = $this->view->render($response, 'empCompartmentReservation.php');
        return $response;
    }
});
$app->get('/emp-train-reservation', function ($request, $response) {
    if (!isset($_SESSION["employee"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {
        $response = $this->view->render($response, 'empTrainReservation.php');
        return $response;
    }
});


$app->get('/employeeResortReservation', function ($request, $response) {
    if (!isset($_SESSION["employee"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {
        $response = $this->view->render($response, 'employeeResortReservation.php');
        return $response;
    }
});
$app->get('/employeeRetireRoomReservation', function ($request, $response) {
    if (!isset($_SESSION["employee"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {
        $response = $this->view->render($response, 'employeeRetireRoomReservation.php');
        return $response;
    }
});
$app->get('/employeeProfile', function ($request, $response) {
    if (!isset($_SESSION["employee"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {
        $response = $this->view->render($response, 'employeeProfile.php');
        return $response;
    }
});

$app->get('/employeeHistory', function ($request, $response) {
    if (!isset($_SESSION["employee"])) {
        $response = $this->view->render($response, 'login.php');
        return $response;

    } else {
        $response = $this->view->render($response, 'employeeHistory.php');
        return $response;
    }
});
// methods

$app->post('/addNewEmployee', function ($request, $response) {

    $json = $request->getParsedBody();

//    $userId = $json["userID"];
    $firstName = $json["firstName"];
    $lastName = $json["lastName"];
    $email = $json["email"];
    $mobile = $json["phoneNumber"];
    $nic = $json["nic"];
    $userName = $json["username"];
    $password = $json["password"];
    $confirmPassword = $json["comfirmPassword"];

    $types = "Employee";
    $points = 2;
if($password === $confirmPassword){
    $employeeDto = new Employee($firstName, $lastName, $userName, $types, $password, $email, $nic, $mobile, $points);
    $manageEmployeeBo = new ManageEmployeeBOImpl();
    $result = $manageEmployeeBo->addEmployee($employeeDto);
    echo $result;
}else{
    echo 10;
}


});

$app->get('/getAllMembers', function ($request, $response) {
    $manageEmployeeBo = new ManageEmployeeBOImpl();
    echo json_encode($manageEmployeeBo->getAllEmployees());
});

$app->post('/isAvailableTrain', function ($request, $response) {

    $json = $request->getParsedBody();

    $from = $json["from"];
    $to = $json["to"];
    $date = $json["date"];

    $trainDTO = new TrainSearch($from, $to, $date);
    $manageEmployeeBo = new ManageEmployeeBOImpl();
    echo json_encode($manageEmployeeBo->searchAvailbleTrain($trainDTO));
});

//----------------------------------------------------------------


$app->post('/isAvailableTrainRequest', function ($request, $response) {

    $json = $request->getParsedBody();

    $from = $json["from"];
    $to = $json["to"];
    $date = $json["date"];

    $trainDTO = new TrainSearch($from, $to, $date);
    $manageEmployeeBo = new ManageEmployeeBOImpl();
    echo json_encode($manageEmployeeBo->searchAvailbleTrain($trainDTO));

});
$app->post('/sendingTrainRequest', function ($request, $response) {

    $json = $request->getParsedBody();
    $trainName = $json["trainName"];
    $from = $json["from"];
    $to = $json["to"];
//    $startTime = $json["statTime"];
    $reserveDate = $json["reserveDateDate"];
//    $endTime = $json["endTime"];
    $sheduleID = $json["shedulID"];
    $trainId = $json["trainID"];
    $type = $json["status"];
    $rid = 0;
    $request = $json["requesting"];
    $class = "B950";


    $trainDTO = new TrainDTO($trainId, $trainName, $class);
    $reservationDTO = new Reservation($rid, $sheduleID, $type);
    $trainReservationDTO = new TrainReservationDetails($trainId, $rid, $reserveDate, $from, $to, $request);

    $trainReservation = new TrainReservationBOImpl();
    $result = $trainReservation->addReservationRequest($reservationDTO, $trainDTO, $trainReservationDTO);
    echo $result;
});
//sending train request by employee
$app->post('/sendingTrainRequestByEmployee', function ($request, $response) {

    $json = $request->getParsedBody();
    $trainName = $json["trainName"];
    $from = $json["from"];
    $to = $json["to"];
//    $startTime = $json["statTime"];
    $reserveDate = $json["reserveDateDate"];
//    $endTime = $json["endTime"];
    $sheduleID = $json["shedulID"];
    $trainId = $json["trainID"];
    $type = $json["status"];
    $rid = 0;
    $request = $json["requesting"];
    $passengerName = $json["passengerName"];
    $class = "B950";


    $trainDTO = new TrainDTO($trainId, $trainName, $class);
    $reservationDTO = new ReservationCore2($rid, $sheduleID,$passengerName, $type);
    $trainReservationDTO = new TrainReservationDetails($trainId, $rid, $reserveDate, $from, $to, $request);

    $trainReservation = new TrainReservationBOImpl();
    $result = $trainReservation->addReservationRequestByEmployee($reservationDTO, $trainDTO, $trainReservationDTO);
    echo $result;
});

//    employee seatBooking transaction

$app->post('/availableSeats', function ($request, $response) {

    //this shouid be searching available seats currently
    $json = $request->getParsedBody();

    $compartmentName = $json["compartmentName"];
    $trainID = $json["trainID"];
    $reservationDate = $json["reserveDateDate"];

    $seatReservationBO = new SeatReservationBOImpl();
    $result = $seatReservationBO->searchSeatReservation($compartmentName, $trainID,$reservationDate);
    echo json_encode($result);
});

$app->post('/goToPayments', function ($request, $response) {

    $json = $request->getParsedBody();

    $type = $json["type"];
    $rid = 0;
    $sheduleID = $json["shedulID"];
    $compartmentID = $json["compartmentID"];
    $bookedSeats = $json["bookSeats"];
//    $trainName = $json["trainName"];
    $from = $json["from"];
    $to = $json["to"];
//    $startTime = $json["statTime"];
    $reserveDate = $json["reserveDateDate"];
//    $endTime = $json["endTime"];

    $trainId = $json["trainID"];

    $reservationDTO = new Reservation($rid, $sheduleID, $type);
    $seatReservationDetail = new SeatReservationDetailDTO($rid, $trainId, $reserveDate, $from, $to);
    $seatReservation = new SeatBookingReservationBOImpl();
    $result = $seatReservation->seatBookingReservation($reservationDTO, $compartmentID, $bookedSeats, $seatReservationDetail);
    echo $result;
});
//getSeatCompartmentID

$app->post('/getSeatCompartmentID', function ($request, $response) {

    $json = $request->getParsedBody();

    $trainId = $json["trainID"];
    $compartmentName = $json["compartmentName"];

    $seatReservation = new SeatBookingReservationBOImpl();
    $compartmentID = $seatReservation->getCompartmentID($compartmentName, $trainId);
    echo $compartmentID['compID'];
});


//sendingCompartmentRequest in here

$app->post('/sendingCompartmentRequest', function ($request, $response) {

    $json = $request->getParsedBody();

    $trainName = $json["trainName"];
    $from = $json["from"];
    $to = $json["to"];
//    $startTime = $json["statTime"];
    $reserveDate = $json["reserveDate"];
//    $endTime = $json["endTime"];
    $sheduleID = $json["shedulID"];
    $trainId = $json["trainID"];
    $type = $json["status"];
    $compartmentName = $json["compartmentName"];
    $rid = 0;
    $compartmentID = 0;
    $request = $json["requesting"];


    $reservationDTO = new Reservation($rid, $sheduleID, $type);
    $compartmentDTO = new CompartmentDTO($compartmentID, $compartmentName, $trainId, $type);
    $compartmentReservationDTO = new CompartmentReservationDTO($compartmentID, $rid, $reserveDate, $from, $to, $request);

    $compartmentReservation = new CompartmentReservationBOImpl();
    $result = $compartmentReservation->addCompartmentReservation($reservationDTO, $compartmentDTO, $compartmentReservationDTO);
    echo $result;
});

//sendingCompartmentRequest in here For employee
$app->post('/sendingCompartmentRequestForEmployee', function ($request, $response) {

    $json = $request->getParsedBody();

    $trainName = $json["trainName"];
    $from = $json["from"];
    $to = $json["to"];
//    $startTime = $json["statTime"];
    $reserveDate = $json["reserveDate"];
//    $endTime = $json["endTime"];
    $sheduleID = $json["shedulID"];
    $trainId = $json["trainID"];
    $type = $json["status"];
    $compartmentName = $json["compartmentName"];
    $rid = 0;
    $compartmentID = 0;
    $request = $json["requesting"];
    $passengerName = $json["passengerName"];


    $reservationDTO = new ReservationCore2($rid, $sheduleID,$passengerName, $type);
    $compartmentDTO = new CompartmentDTO($compartmentID, $compartmentName, $trainId, $type);
    $compartmentReservationDTO = new CompartmentReservationDTO($compartmentID, $rid, $reserveDate, $from, $to, $request);

    $compartmentReservation = new CompartmentReservationBOImpl();
    $result = $compartmentReservation->addCompartmentReservationFromEmployee($reservationDTO, $compartmentDTO, $compartmentReservationDTO);
    echo $result;
});

$app->get('/acceptingTrainRequest', function ($request, $response) {

    $json = $request->getParsedBody();

    $reservationID = $json["rid"];

    $trainReservationBo = new TrainReservationBOImpl();
    $result = $trainReservationBo->acceptingTrainRequest($reservationID);
    echo $result;
});
$app->post('/OnFootAvailableSpace', function ($request, $response) {

    $json = $request->getParsedBody();

    $compartmantName = $json["compartmentName"];
    $trainID = $json["trainID"];

    $compartmentBo = new CompartmentBOImpl();
    $result = $compartmentBo->onFootAvailableSpace($compartmantName,$trainID);
    echo json_encode($result);
});
$app->post('/OnFootBooking', function ($request, $response) {

    $json = $request->getParsedBody();

    $compartmantName = $json["compartmentName"];
    $trainID = $json["trainID"];
    $bookCount = $json["bookSpace"];

    $compartmentBo = new CompartmentBOImpl();
    $result = $compartmentBo->updateCompartmentExtra($compartmantName,$trainID,$bookCount);
    echo $result;
});

$app->get('/getAllEmployeeDetailByEmpID', function ($request, $response) {
    $manageEmployeeBo = new ManageEmployeeBOImpl();
    echo json_encode($manageEmployeeBo->getAllEmployeeDetailByEmpID());
});
//for employee history

$app->get('/getAllEmployeeSeatReservations', function ($request, $response) {
    $seatReservationRepo = new SeatReservationBOImpl();
    echo json_encode($seatReservationRepo->getAllEmployeeSeatReservations());
});

$app->get('/getAllEmployeeCompartmentReservations', function ($request, $response) {
    $compartmentReservationRepo = new CompartmentReservationBOImpl();
    echo json_encode($compartmentReservationRepo->getAllCompartmentForEmployeeReservations());
});

$app->get('/getAllEmployeeTrainReservations', function ($request, $response) {
    $trainReservationRepo = new TrainReservationBOImpl();
    echo json_encode($trainReservationRepo->getAllEmployeeTrainReservations());
});
$app->post('/updateEmployeeDetail', function ($request, $response) {

    $json = $request->getParsedBody();

    $firstName = $json["firstName"];
    $lastName = $json["lastName"];
    $userName = $json["username"];
    $types = "Employee";
    $password = $json["password"];
    $email = $json["email"];
    $nic = $json["nic"];
    $mobile = $json["mobile"];
    $points = 0;


    $manageEmployeeBo = new ManageEmployeeBOImpl();
    $employeeDTO = new Employee($firstName, $lastName, $userName, $types, $password, $email, $nic, $mobile, $points);
    $result = $manageEmployeeBo->updateEmployee($employeeDTO);
    echo $result;
});
$app->post('/cancelThisReservation', function ($request, $response) {

    $json = $request->getParsedBody();


    $reservationID = $json["reservationID"];

    $reservationBo = new ReservationBOImpl();
    $result = $reservationBo->deleteReservation($reservationID);
    echo $result;
});

