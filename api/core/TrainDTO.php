<?php

class TrainDTO
{
    private $trainID;
    private $name;
    private $class;

    /**
     * TrainDTO constructor.
     * @param $trainID
     * @param $name
     * @param $class
     */
    public function __construct($trainID, $name, $class)
    {
        $this->trainID = $trainID;
        $this->name = $name;
        $this->class = $class;
    }

    /**
     * @return mixed
     */
    public function getTrainID()
    {
        return $this->trainID;
    }

    /**
     * @param mixed $trainID
     */
    public function setTrainID($trainID): void
    {
        $this->trainID = $trainID;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     */
    public function setClass($class): void
    {
        $this->class = $class;
    }


}