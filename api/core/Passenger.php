<?php

namespace api\core;
class Passenger
{

    private $passengerFirstName;
    private $passengerLastname;
    private $passengerName;
    private $type;
    private $password;
    private $email;
    private $nic;
    private $phone;
    private $points;

    /**
     * Passenger constructor.
     * @param $passengerFirstName
     * @param $passengerLastname
     * @param $passengerName
     * @param $type
     * @param $password
     * @param $email
     * @param $nic
     * @param $phone
     * @param $points
     */
    public function __construct($passengerFirstName, $passengerLastname, $passengerName, $type, $password, $email, $nic, $phone, $points)
    {
        $this->passengerFirstName = $passengerFirstName;
        $this->passengerLastname = $passengerLastname;
        $this->passengerName = $passengerName;
        $this->type = $type;
        $this->password = $password;
        $this->email = $email;
        $this->nic = $nic;
        $this->phone = $phone;
        $this->points = $points;
    }

    /**
     * @return mixed
     */
    public function getPassengerFirstName()
    {
        return $this->passengerFirstName;
    }

    /**
     * @param mixed $passengerFirstName
     */
    public function setPassengerFirstName($passengerFirstName): void
    {
        $this->passengerFirstName = $passengerFirstName;
    }

    /**
     * @return mixed
     */
    public function getPassengerLastname()
    {
        return $this->passengerLastname;
    }

    /**
     * @param mixed $passengerLastname
     */
    public function setPassengerLastname($passengerLastname): void
    {
        $this->passengerLastname = $passengerLastname;
    }

    /**
     * @return mixed
     */
    public function getPassengerName()
    {
        return $this->passengerName;
    }

    /**
     * @param mixed $passengerName
     */
    public function setPassengerName($passengerName): void
    {
        $this->passengerName = $passengerName;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getNic()
    {
        return $this->nic;
    }

    /**
     * @param mixed $nic
     */
    public function setNic($nic): void
    {
        $this->nic = $nic;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     */
    public function setPoints($points): void
    {
        $this->points = $points;
    }

}