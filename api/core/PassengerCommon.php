<?php

namespace api\core;
class PassengerCommon
{

    private $fistName;
    private $lastName;
    private $userName;
    private $password;
    private $email;
    private $nic;
    private $mobile;

    /**
     * PassengerCommon constructor.
     * @param $fistName
     * @param $lastName
     * @param $userName
     * @param $password
     * @param $email
     * @param $nic
     * @param $mobile
     */

    public function __construct($fistName, $lastName, $userName, $password, $email, $nic, $mobile)
    {
        $this->fistName = $fistName;
        $this->lastName = $lastName;
        $this->userName = $userName;
        $this->password = $password;
        $this->email = $email;
        $this->nic = $nic;
        $this->mobile = $mobile;
    }

    /**
     * @return mixed
     */
    public function getFistName()
    {
        return $this->fistName;
    }

    /**
     * @param mixed $fistName
     */
    public function setFistName($fistName): void
    {
        $this->fistName = $fistName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getNic()
    {
        return $this->nic;
    }

    /**
     * @param mixed $nic
     */
    public function setNic($nic): void
    {
        $this->nic = $nic;
    }

    /**
     * @return mixed
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param mixed $mobile
     */
    public function setMobile($mobile): void
    {
        $this->mobile = $mobile;
    }
}