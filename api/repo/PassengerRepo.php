<?php

namespace api\repo;

use api\core\Passenger;
use api\core\PassengerCommon;

require_once __DIR__ . "/../core/Passenger.php";
require_once __DIR__ . "/../core/PassengerCommon.php";

interface PassengerRepo
{

    public function setConnection(mysqli $connection);

    public function addPassenger(Passenger $passenger): bool;

    public function updatePassenger(PassengerCommon $passenger): bool;

    public function searchPassenger(string $id): array;

    public function getAllPassenger(): array;

    public function updatePassengerPoint($userName, $points): bool;

    public function updatePassengerPointToIncrease($userName, $points): bool;

    public function updatePassengerFromEmployee(PassengerCommon $passenger): bool;

    public function getEmailAddressFromPaseenger(): array;

    public function getEmailAddressPaseenger(): array;

    public function getAllPassengerUserNames(): array;

    public function getPassengerEmail(string $userName): array;
}