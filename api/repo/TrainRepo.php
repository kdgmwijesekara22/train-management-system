<?php

require_once __DIR__."/../core/TrainDTO.php";

interface TrainRepo
{
    public function setConnection(mysqli $connection);

    public function getAllTrains():array ;

    public function addTrain(TrainDTO $trainDTO):bool ;

    public function updateTrain(TrainDTO $trainDTO):bool;

}