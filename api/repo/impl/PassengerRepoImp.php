<?php

namespace api\repo\impl;

use api\core\Passenger;
use api\core\PassengerCommon;
use api\repo\PassengerRepo;

require_once __DIR__ . "/../../core/Passenger.php";
require_once __DIR__ . "/../../core/PassengerCommon.php";
require_once __DIR__ . "/../../db/DBConnection.php";
require_once __DIR__ . "/../PassengerRepo.php";

class PassengerRepoImp implements PassengerRepo
{
    private $connection;

    public function setConnection(mysqli $connection)
    {
        $this->connection = $connection;
    }

    public function addPassenger(Passenger $passenger): bool
    {
        // TODO: Implement addPassenger() method.
    }

    public function updatePassenger(PassengerCommon $passenger): bool
    {
        // TODO: Implement updatePassenger() method.
    }

    public function searchPassenger(string $id): array
    {
        // TODO: Implement searchPassenger() method.
    }

    public function getAllPassenger(): array
    {
        $resultset = $this->connection->query("select * from _user");
        return $resultset->fetch_all();
    }

    public function updatePassengerPoint($userName, $points): bool
    {
        // TODO: Implement updatePassengerPoint() method.
    }

    public function updatePassengerPointToIncrease($userName, $points): bool
    {
        // TODO: Implement updatePassengerPointToIncrease() method.
    }

    public function updatePassengerFromEmployee(PassengerCommon $passenger): bool
    {
        // TODO: Implement updatePassengerFromEmployee() method.
    }

    public function getEmailAddressFromPaseenger(): array
    {
        // TODO: Implement getEmailAddressFromPaseenger() method.
    }

    public function getEmailAddressPaseenger(): array
    {
        // TODO: Implement getEmailAddressPaseenger() method.
    }

    public function getAllPassengerUserNames(): array
    {
        $resultset = $this->connection->query("SELECT _user.userName FROM _user WHERE _user.types = 'Passenger'");
        return $resultset->fetch_all();
    }

    public function getPassengerEmail(string $userName): array
    {
        // TODO: Implement getPassengerEmail() method.
    }
}