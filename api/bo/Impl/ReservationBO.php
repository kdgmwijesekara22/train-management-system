<?php

interface ReservationBO
{

    public function cancelCompartmentRequestBeforeDate($reservationID ,$currentDate):bool;

    public function cancelTrainRequestBeforeDate($reservationID ,$currentDate):bool;

    public function getLastReservationIDForReferance(): array;

    public function deleteReservation($reservationID): bool;
}