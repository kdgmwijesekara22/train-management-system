<?php

use api\core\Passenger;
use api\core\PassengerCommon;
use api\db\DBConnection;
use api\repo\impl\PassengerRepoImp;

require_once __DIR__ . "/../PassengerBO.php";
require_once __DIR__ . "/../../../core/Passenger.php";
require_once __DIR__ . "/../../../core/PassengerCommon.php";
require_once __DIR__ . "/../../../repo/impl/PassengerRepoImp.php";
require_once __DIR__ . "/../../../db/DBConnection.php";
class PassengerBOImpl implements PassengerBO{

    private $passengerRepo;

    public function addPassenger(Passenger $passenger): bool
    {
        $this->passengerRepo = new PassengerRepoImp();
        $connection = (new DBConnection())->getDBConnection();
        $this->passengerRepo->setConnection($connection);
        $result =  $this->passengerRepo->addPassenger($passenger);
        return $result;
    }

    public function deletePassenger($userID): bool
    {
        // TODO: Implement deletePassenger() method.
    }

    public function searchPassenger($userID): array
    {
        $connection = (new DBConnection())->getDBConnection();
        $passengerRepo = new PassengerRepoImp();
        $passengerRepo->setConnection($connection);
        return $passengerRepo->searchPassenger($userNIC);
    }

    public function updatePassenger(PassengerCommon $passenger): bool
    {
        $passengerRepo = new PassengerRepoImp();
        $connection = (new DBConnection())->getDBConnection();
        $passengerRepo->setConnection($connection);
        $result = $passengerRepo->updatePassenger($passenger);
        return $result;
    }

    public function getAllPassengers(): array
    {
        $connection = (new DBConnection())->getDBConnection();
        $passengerRepo = new PassengerRepoImp();
        $passengerRepo->setConnection($connection);
        return $passengerRepo->getAllPassenger();
    }

    public function updatePassengerPoint($userName, $points): bool
    {
        $connection = (new DBConnection())->getDBConnection();
        $passengerRepo = new PassengerRepoImp();
        $passengerRepo->setConnection($connection);
        return $passengerRepo->updatePassengerPoint($userName,$points);
    }

    public function updatePassengerFromEmployee(PassengerCommon $passenger): bool
    {
        $connection = (new DBConnection())->getDBConnection();
        $passengerRepo = new PassengerRepoImp();
        $passengerRepo->setConnection($connection);
        return $passengerRepo->updatePassengerFromEmployee($passenger);
    }

    public function getEmailAddressFromPaseenger(): array
    {
        $connection = (new DBConnection())->getDBConnection();
        $passengerRepo = new PassengerRepoImp();
        $passengerRepo->setConnection($connection);
        return $passengerRepo->getEmailAddressFromPaseenger();
    }

    public function getEmailAddressPaseenger(): array
    {
        $connection = (new DBConnection())->getDBConnection();
        $passengerRepo = new PassengerRepoImp();
        $passengerRepo->setConnection($connection);
        return $passengerRepo->getEmailAddressPaseenger();
    }

    public function getAllPassengerUserNames(): array
    {
        $connection = (new DBConnection())->getDBConnection();
        $passengerRepo = new PassengerRepoImp();
        $passengerRepo->setConnection($connection);
        return $passengerRepo->getAllPassengerUserNames();
    }

    public function getPassengerEmail(string $userName): array
    {
        $connection = (new DBConnection())->getDBConnection();
        $passengerRepo = new PassengerRepoImp();
        $passengerRepo->setConnection($connection);
        return $passengerRepo->getPassengerEmail($userName);
    }
}