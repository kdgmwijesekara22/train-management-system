<?php

use api\core\Passenger;
use api\core\PassengerCommon;

require_once __DIR__ . '/../../core/Passenger.php';
require_once __DIR__ . '/../../core/PassengerCommon.php';

interface PassengerBO
{

    public function addPassenger(Passenger $passenger): bool;

    public function deletePassenger($userID): bool;

    public function searchPassenger($userID): array;

    public function updatePassenger(PassengerCommon $passenger): bool;

    public function getAllPassengers(): array;

    public function updatePassengerPoint($userName, $points): bool;

    public function updatePassengerFromEmployee(PassengerCommon $passenger): bool;

    public function getEmailAddressFromPaseenger(): array;

    public function getEmailAddressPaseenger(): array;

    public function getAllPassengerUserNames(): array;

    public function getPassengerEmail(string $userName): array;
}

