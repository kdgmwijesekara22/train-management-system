<?php

namespace api\db;
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 2/12/2019
 * Time: 10:23 AM
 */
class DBConnection
{
    private $host = "localhost";
    private $userName = "root";
    private $password = "1234";
    private $database = "trainrs";

    private $port = "3306";

    private $connection;

    /**
     * DBConnection constructor.
     */
    public function __construct()
    {
        $this->connection = new mysqli(
            $this->host,
            $this->userName,
            $this->password,
            $this->database,
            $this->port
        );
    }

    public function getDBConnection()
    {
        return $this->connection;
    }
}